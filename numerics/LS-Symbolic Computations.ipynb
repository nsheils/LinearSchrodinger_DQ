{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear Schrödinger: Symbolic Computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Problem\n",
    "\n",
    "Study $$(i\\partial_t)+\\partial_x^2)q(x,t)=0$$ on $(x,t)\\in [0,L] \\times[0,T)$ with the initial condition $$q(x,0)=q_0(x)$$ and pseudoperiodic boundary conditions $$\\partial_x^k q(L,t)=\\beta_k q(0,t),~~k=0,1.$$\n",
    "\n",
    "In order to have an energy conserving system we must choose boundary conditions such that $\\beta_0\\beta_1=1.$  In what follows we let $\\beta_0=1/\\beta_1$ and $\\beta:=\\beta_1$ for simplicity.  Hence, our boundary conditions are\n",
    "\n",
    "$q(L,t)=\\frac{1}{\\beta} q(0,t),$\n",
    "\n",
    "$q_x(L,t)=\\beta q(L,t).$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The UTM\n",
    "\n",
    "We have the local relation $$(e^{-ikx +\\omega t}q)_t-(e^{-ikx+\\omega t}(iq_x-kq))_x=0$$ with $\\omega(k)=ik^2.$\n",
    "\n",
    "Integrating around the $(x,t)$ domain of dependence we have the global relation\n",
    "$$0=\\hat{q}_0(k)+e^{\\omega T}\\hat{q}(k,T)+e^{-ikL}(h_1(\\omega,T)-kh_0(\\omega,T))-(g_1(\\omega,T)-kg_0(\\omega,T)),$$ where\n",
    "\n",
    "$g_j(\\omega,t)=\\int_0^t e^{\\omega s}\\partial_x^j u(0,s)ds,$\n",
    "\n",
    "$h_j(\\omega,t)=\\int_0^t e^{\\omega s}\\partial_x^j u(L,s)ds,$\n",
    "\n",
    "$\\hat{q}(k,t)=\\int_0^L e^{-ikx}q(x,t)dx,$\n",
    "\n",
    "Using the boundary conditions, global relation, and the evaluation of the global relation at $-k$ we have a $4\\times4$ linear system:\n",
    "\n",
    "$$\\mathcal{A}X=Y$$\n",
    "\n",
    "where\n",
    "$$\\mathcal{A}=\\left(\\begin{array}{cccc} e^{-ikL} &-k e^{-ikL}&-1&k\\\\e^{ikL} &k e^{ikL}&-1&-k\\\\1&0&-\\beta&0\\\\0&1&0&-1/\\beta\\end{array}\\right),$$\n",
    "$$X=\\left(h_1(\\omega,t),h_0(\\omega,t),g_1(\\omega,t), g_0(\\omega,t)\\right)^\\top,$$\n",
    "$$Y=\\left(-\\hat{q}_0(k), -\\hat{q}_0(-k),0,0\\right)^\\top,$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$0$$"
      ],
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# dependencies\n",
    "using PyPlot\n",
    "using SymPy\n",
    "using ProgressMeter\n",
    "#using ApproxFun\n",
    "\n",
    "k = symbols(\"k\", complex=true, nonzero=true); #we must always avoid the origin...this might not be ok\n",
    "L = symbols(\"L\", positive=true, real=true);\n",
    "beta = symbols(\"beta\", nonzero=true, real=true);\n",
    "q0hat_pos=symbols(\"q0hat_pos\");\n",
    "q0hat_neg=symbols(\"q0hat_neg\");\n",
    "x=symbols(\"x\", positive=true, real=true);\n",
    "t=symbols(\"t\", positive=true, real=true);\n",
    "A=[exp(-im*k*L) -k*exp(-im*k*L) -1 k; exp(im*k*L) k*exp(im*k*L) -1 -k; 1 0 -beta 0; 0 1 0 -1/beta];\n",
    "Y=[-q0hat_pos; -q0hat_neg; 0; 0];\n",
    "gh=\\(A,Y);\n",
    "h1=simplify(gh[1]);\n",
    "h0=simplify(gh[2]);\n",
    "g1=simplify(gh[3]);\n",
    "g0=simplify(gh[4]);\n",
    "\n",
    "Delta=simplify(-beta/k*det(A))  #scaling by -beta/k to match paper\n",
    "zetam=simplify(-(h1-k*h0)*Delta)\n",
    "zetap=simplify(-(g1-k*g0)*Delta)\n",
    "\n",
    "#check..should equal 0\n",
    "simplify(zetap-exp(-im*k*L)*zetam+q0hat_pos*Delta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\left\\{n - \\frac{1}{8}\\; |\\; n \\in \\mathbb{Z}\\right\\} \\cup \\left\\{n + \\frac{1}{8}\\; |\\; n \\in \\mathbb{Z}\\right\\}$$"
      ],
      "text/plain": [
       "{n - 1/8 | n in Integers()} U {n + 1/8 | n in Integers()}"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "kzero=simplify(factor(solveset(Delta,k)));\n",
    "\n",
    "mint=-1/(2*PI*Delta)*exp(im*k*(x-L)-im*k^2*t)*zetam;\n",
    "pint=-1/(2*PI*Delta)*exp(im*k*x-im*k^2*t)*zetap;\n",
    "\n",
    "# parameters of problem\n",
    "L = 2*PI;\n",
    "beta = 1+sqrt(SymPy.Sym(2));\n",
    "#beta = 1+sqrt(x)|>subs(x,2); #This is a strange way to write beta but it keeps it as a symbol\n",
    "rts_btwn=100; #find roots between -rts_btwn and rts_btwn\n",
    "tol=10e-13; #Find roots to this tolerance\n",
    "\n",
    "#initial condition\n",
    "#step function\n",
    "q0(x)=Heaviside(x-L/2);\n",
    "q0hat(k)=SymPy.integrate(q0(x)*exp(-im*k*x),x,0,L)\n",
    "\n",
    "free_symbols(mint)\n",
    "\n",
    "intm=lambdify(mint(t,x,q0hat(k),k,beta,q0hat(-k),L),[k,t,x]) #argument order determined by free_symbols\n",
    "intp=lambdify(pint(t,x,q0hat(k),k,beta,q0hat(-k),L),[k,t,x]) #argument order determined by free_symbols\n",
    "\n",
    "krts=simplify(kzero(L,beta))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that all the roots of $\\Delta$ are on the real line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAh4AAAGiCAYAAACoI8/ZAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAPYQAAD2EBqD+naQAAIABJREFUeJzt3Xt0VOW9//FPAuSCMMEI5HII95ZLEbBBQmh/NkgU1KNwpFQsqwQasHiILYalEJcarKulVhQsUtGjED2LVKQIKCgWo0DVABLgKBxISw6SCJmgYhIIkGAyvz/SDJlkJpmZzN4zk7xfa80is+eZvZ/5MrOfz+zZlxCbzWYTAACACUL93QEAANBxEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkMDR4vvPCCRo4cKYvFIovFouTkZL377rtGLhIAgHZpz549uvPOOxUfH6+QkBBt2bKl1efs2rVLP/zhDxUeHq7BgwcrJyenWZuNGzdq6NChioiI0PXXX6933nnHgN5fZWjw6NOnj/7whz+ooKBABw4c0M0336wpU6bo6NGjRi4WAIB2p6qqSqNGjdLq1avdan/y5EndcccdmjBhgg4fPqyFCxdq7ty5eu+99+xtPvnkE917771KT0/XoUOHNHXqVE2dOlVHjhwx6mUoxOyLxEVHR+vpp59Wenq6mYsFAKDdCAkJ0ebNmzV16lSXbRYvXqzt27c7hIgZM2aovLxcO3bskCTdc889qqqq0rZt2+xtxo0bp9GjR2vNmjWG9L2zIXN1ora2Vhs3blRVVZWSk5OdtqmurlZ1dbX9fl1dnc6dO6frrrtOISEhZnUVABCEbDabzp8/r/j4eIWGGrdB//Lly6qpqWnzfGw2W7OxLTw8XOHh4W2etyTl5+crNTXVYdqkSZO0cOFChzaZmZnN2rjzM463DA8en3/+uZKTk3X58mV169ZNmzdv1vDhw522XbZsmZ544gmjuwQAaMdKSkrUp08fQ+Z9+fJlRV0Xr5qL37Z5Xt26ddOFCxccpmVnZ2vp0qVtnrckWa1WxcTEOEyLiYlRZWWlLl26pMjISJdtrFarT/rgjOHBY8iQITp8+LAqKir017/+VWlpadq9e7fT8JGVleWQvCoqKtS3b18Vf/aMLN0jJUm2A5/aHz//zkn730f+3sWr/o34f1fsf3e/fYD975AxN17tR7cw+9+flNbZ/956wnGZ1vcjveqDJMWmXrL/PWXw1T6Nj7ua2qMuXE3YruogeVcLb+vQtAZSx6yD5Lv3Q+MaSObXoXENpMCpg3S1Fv6og3S1FoFSB+lqLdypgy/Wk5Lz94TZ60mpeR3On7+kEQMfUPfu3b1eRmtqampUc/Fb/XjuWnUO6+r1fL6ruaiPXv6lSkpKZLFY7NN9tbUjkBkePMLCwjR48GBJUmJioj799FM999xzevHFF5u1dbWJydI98mrw6Hr1zR0SdrX713Ty7gNlCbu6i0v3xvPufvXDYet+tU9dL9Ta/w675mp7Seoc7v0HKuyaq5vbuna/ugKxWDpd/Tvk6t+u6iB5Vwtv69C0BlLHrEN9333zfmhcA8n8OjSugRQ4dZCu1sIfdZCu1iJQ6iBdrYU7dfDFelJy/p4wez0puf5smPHTfOewruoc7n3waNBw1KcRYmNjVVZW5jCtrKxMFotFkZGRLbaJjY01pE+SH87jUVdX57AfBwAA8L3k5GTl5eU5TNu5c6fDfpbutPE1Q7d4ZGVl6bbbblPfvn11/vx55ebmateuXQ6H8gAAgNZduHBBJ06csN8/efKkDh8+rOjoaPXt21dZWVk6ffq0XnvtNUnS/Pnz9fzzz+vhhx/WL3/5S33wwQd64403tH37dvs8fvOb3+gnP/mJnnnmGd1xxx16/fXXdeDAAb300kuGvQ5Dg8fZs2c1a9YslZaWKioqSiNHjtR7772nW265xcjFAgDQ7hw4cEATJkyw32/YJzItLU05OTkqLS1VcXGx/fEBAwZo+/btevDBB/Xcc8+pT58+evnllzVp0iR7m/Hjxys3N1ePPvqoHnnkEX3ve9/Tli1bNGLECMNeh6HB45VXXjFy9gAAdBgpKSlq6dRbzs5KmpKSokOHDrU43+nTp2v69Olt7Z7buFYLAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDQEDwAAYBqCBwAAMA3BAwAAmIbgAQAATEPwAAAApiF4AAAA0xA8AACAaQgeAADANAQPAABgGoIHAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAIEisXr1a/fv3V0REhJKSkrR//36XbWfPnq2QkJBmtx/84Af2Njk5Oc0ej4iIMPQ1EDwAAAgCGzZsUGZmprKzs3Xw4EGNGjVKkyZN0tmzZ522f+6551RaWmq/lZSUKDo6WtOnT3doZ7FYHNqdOnXK0NdB8AAAIAg8++yzmjdvnubMmaPhw4drzZo16tq1q9auXeu0fVRUlGJjY+23AwcO6Ntvv9WcOXMc2oWEhDi0i4mJMfR1EDwAAPCTyspKh1t1dbXTdjU1NSooKFBqaqp9WmhoqFJTU5Wfn+/Wsl555RWlpqaqX79+DtMvXLigfv36KSEhQVOmTNHRo0e9f0Fu6Gzo3AEAaIdiUy8p7JoQr59fU3VJ+rOUkJDgMD07O1tLly5t1v7rr79WbW1ts60RMTExOn78eKvLO3PmjN59913l5uY6TB8yZIjWrl2rkSNHqqKiQsuXL9f48eN19OhR9enTx/MX5gaCBwAAflJSUiKLxWK/Hx4ebshyXn31VfXo0UNTp051mJ6cnKzk5GT7/fHjx2vYsGF68cUX9eSTTxrSF4IHAAB+YrFYHIKHKz179lSnTp1UVlbmML2srEyxsbEtPtdms2nt2rX6xS9+obCwsBbbdunSRTfccINOnDjReue9xD4eAAAEuLCwMCUmJiovL88+ra6uTnl5eQ5bLJzZvXu3Tpw4ofT09FaXU1tbq88//1xxcXFt7rMrbPEAACAIZGZmKi0tTWPGjNHYsWO1cuVKVVVV2Y9SycrK0unTp/Xaa685PO+VV15RUlKSRowY0Wyev/3tbzVu3DgNHjxY5eXlevrpp3Xq1CnNnTvXsNdB8AAAIAjcc889+uqrr/T444/LarVq9OjR2rFjh32H09LSUhUXFzs8p6KiQps2bdJzzz3ndJ7ffvut5s2bJ6vVqmuvvVaJiYn65JNPNHz4cMNeB8EDAIAgkZGRoYyMDKeP5eTkNJsWFRWlixcvupzfihUrtGLFCl91zy3s4wEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDSGBo9ly5bpxhtvVPfu3dW7d29NnTpVhYWFRi4SAAAEMEODx+7du7VgwQLt3btXO3fu1JUrV3TrrbeqqqrKyMUCAIAAZeh5PHbs2OFwPycnR71791ZBQYFuuummZu2rq6sdLglcWVlpZPcAAIDJTN3Ho6KiQpIUHR3t9PFly5YpKirKfmt6uWAAABDcTAsedXV1WrhwoX70ox85PV+8VH+e+YqKCvutpKTErO4BAAATmHbK9AULFujIkSP66KOPXLYJDw9XeHi4WV0CAAAmMyV4ZGRkaNu2bdqzZ4/69OljxiIBAEAAMjR42Gw2PfDAA9q8ebN27dqlAQMGGLk4AAAQ4AwNHgsWLFBubq62bt2q7t27y2q1Sqq/Wl5kZKSRiwYAAAHI0J1LX3jhBVVUVCglJUVxcXH224YNG4xcLAAACFCG/9QCAADQgGu1AAAA0xA8AACAaQgeAADANAQPAABgGoIHAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDQEDwAAgsTq1avVv39/RUREKCkpSfv373fZdteuXQoJCWl2s1qtDu02btyooUOHKiIiQtdff73eeecdQ18DwQMAgCCwYcMGZWZmKjs7WwcPHtSoUaM0adIknT17tsXnFRYWqrS01H7r3bu3/bFPPvlE9957r9LT03Xo0CFNnTpVU6dO1ZEjRwx7HQQPAACCwLPPPqt58+Zpzpw5Gj58uNasWaOuXbtq7dq1LT6vd+/eio2Ntd9CQ68O/c8995wmT56shx56SMOGDdOTTz6pH/7wh3r++ecNex0EDwAA/KSystLhVl1d7bRdTU2NCgoKlJqaap8WGhqq1NRU5efnt7iM0aNHKy4uTrfccos+/vhjh8fy8/Md5ilJkyZNanWebUHwAADATxISEhQVFWW/LVu2zGm7r7/+WrW1tYqJiXGYHhMT02yfjQZxcXFas2aNNm3apE2bNikhIUEpKSk6ePCgvY3VavVonr7Q2bA5AwDQTk0ZfEVdu9d4/fyL56/odUklJSWyWCz26eHh4T7oXb0hQ4ZoyJAh9vvjx49XUVGRVqxYof/+7//22XI8RfAAAMBPLBaLQ/BwpWfPnurUqZPKysocppeVlSk2Ntbt5Y0dO1YfffSR/X5sbGyb5+kpfmoBACDAhYWFKTExUXl5efZpdXV1ysvLU3JystvzOXz4sOLi4uz3k5OTHeYpSTt37vRonp5iiwcAAEEgMzNTaWlpGjNmjMaOHauVK1eqqqpKc+bMkSRlZWXp9OnTeu211yRJK1eu1IABA/SDH/xAly9f1ssvv6wPPvhAf/vb3+zz/M1vfqOf/OQneuaZZ3THHXfo9ddf14EDB/TSSy8Z9joIHgAABIF77rlHX331lR5//HFZrVaNHj1aO3bssO8cWlpaquLiYnv7mpoaLVq0SKdPn1bXrl01cuRIvf/++5owYYK9zfjx45Wbm6tHH31UjzzyiL73ve9py5YtGjFihGGvg+ABAECQyMjIUEZGhtPHcnJyHO4//PDDevjhh1ud5/Tp0zV9+nRfdM8t7OMBAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDQEjyB3/q0ivy5/2vdr/Lp8AEBwIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJjG0OCxZ88e3XnnnYqPj1dISIi2bNli5OIAAECAMzR4VFVVadSoUVq9erWRiwEAAEGis5Ezv+2223TbbbcZuQgAABBEDA0enqqurlZ1dbX9fmVlpR97AwAAfC2gdi5dtmyZoqKi7LeEhAR/dwkAAPhQQAWPrKwsVVRU2G8lJSX+7hIAAPChgPqpJTw8XOHh4f7uBgAEjDM7IhU/+ZK/uwH4TEBt8QD8aVQKV9oFAKMZusXjwoULOnHihP3+yZMndfjwYUVHR6tv375GLhoAAAQgQ4PHgQMHNGHCBPv9zMxMSVJaWppycnKMXDQAAAhAhgaPlJQU2Ww2IxcBAACCCPt4oF04/1aRv7sAAHADwQMAAJiG4AEAAExD8AAAAKYheAAAANMEbfBgZ0IAQEezevVq9e/fXxEREUpKStL+/ftdtn3zzTd1yy23qFevXrJYLEpOTtZ7773n0CYnJ0chISEOt4iICENfQ9AGD6C9OrMj0t9dABCANmzYoMzMTGVnZ+vgwYMaNWqUJk2apLNnzzptv2fPHt1yyy165513VFBQoAkTJujOO+/UoUOHHNpZLBaVlpbab6dOnTL0dQTUtVoAAOhIKisrHe63dM2yZ599VvPmzdOcOXMkSWvWrNH27du1du1aLVmypFn7lStXOtz//e9/r61bt+rtt9/WDTfcYJ8eEhKi2NjYtr4Ut7HFA0DQ2XOm1t9dAHwiISFBUVFR9tuyZcuctqupqVFBQYFSU1Pt00JDQ5Wamqr8/Hy3llVXV6fz588rOjraYfqFCxfUr18/JSQkaMqUKTp69Kj3L8gNHXqLBxcFAwB4Y3xcqCyWTl4/v7Ky/nt/SUmJLBaLfbqrrR1ff/21amtrFRMT4zA9JiZGx48fd2uZy5cv14ULF/Szn/3MPm3IkCFau3atRo4cqYqKCi1fvlzjx4/X0aNH1adPH09flls6dPAAAMCfLBaLQ/AwSm5urp544glt3bpVvXv3tk9PTk5WcnKy/f748eM1bNgwvfjii3ryyScN6QvBAwCAANezZ0916tRJZWVlDtPLyspa3T/j9ddf19y5c7Vx40aHn2qc6dKli2644QaHK8v7Gvt4AABc4ifpwBAWFqbExETl5eXZp9XV1SkvL89hi0VTf/nLXzRnzhz95S9/0R133NHqcmpra/X5558rLi7OJ/12hi0eAAAEgczMTKWlpWnMmDEaO3asVq5cqaqqKvtRLllZWTp9+rRee+01SfU/r6Slpem5555TUlKSrFarJCkyMlJRUVGSpN/+9rcaN26cBg8erPLycj399NM6deqU5s6da9jrIHgACEhndkQqfvIln8zrf3aF+WQ+gD/dc889+uqrr/T444/LarVq9OjR2rFjh32H09LSUhUXF9vbv/TSS/ruu++0YMECLViwwD49LS1NOTk5kqRvv/1W8+bNk9Vq1bXXXqvExER98sknGj58uGGvg+ABAPBKSNI4f3ehw8nIyFBGRobTxxrCRINdu3a1Or8VK1ZoxYoVPuiZ+9jHAwAAmIbgAQQRTpyFYDPt++ycCkcEj3bGm9+y2WsdAGAWggcAGIBv+oBzBA8AQFDgys3tA8EDANzQ/a5B/u5CQKAOaCuCBwAAMA3BAwhwm/7h/cmv/HHiLPZtwE3x3l+1Fe1fUAUP2769TqdzVkI0xXsCAAJTUAUPdFycvwJoH3x1GnwEL4IHAAAwTYcNHk1PmsWe2vWoAwDASB02eMA4bEoFALhC8PAhBtz2w5stP02P5vDH++H8W0WmLxMAPEHwCEKuju4BAlEwBvJAu34RZ+y8iloEP4JHgODD1Fxr568ggPke+z61TUvnryCAAfUIHk6EJI3zdxdMw4qlXjDXgQAGIJgQPAKQt2eq9NdJszhTJQBPBOPWH/hO0AcPbwZbX327ZcAFAoeRW374yQnwnaAPHjCOP35yYl+X4GTUtTmavh/c3RrI0T3NcRkBBAqCRwcWzPs1tHcEsPbBqEOs23LhwLbwdssPW4fRGMFDvt2MGsy/XXpSh9a+4QZTHVo7kqOlLT9N6xAI5/Lwli+DKHXwPbPDaKDWoTF/BTC0TYcLHm39MJl9uWd3L47m782o/vxG03Szur9rYQR/rGA9CWCBrK3vh2CuQyD/5BRMYRS+1eGCh68F6ybEYPg2A7RHwTrgBnMAQ2Dp8MHDiL3VzVqxBPK3GSk4VrBmBDB/vR88+abfnurQFkbUwZdfTsz6ucUXdWjtZ0h0XEEZPLwdcPmWX89ZHThcsJ4v6sAKtl5HrYM7P8cGQwgzA3XomIIyeBgpGDcfGrFPgzd1CLaBhiDqmjcBrD0OuATywMRRX8GtwwQPI7/lB9OAy9aOeu7WwZ0AFuwDrjsBLBgDuaeoQz3WETBahwgevhxkJOcDjS8OHzQ6xbv7Dd/fdWir1l6nr+vgjLMwanYt3HmdRg8ygVAHd/i6Du7s3xDMdfB2HREsdYCxgjp4uPMTg68/SC0JhEHXFVeDkBF7qgdCHVy9XqPq4E4I84QZNTPis+FuHdx9fYFUh7YK9EHXrHVloNcBxgvq4NEaI1corjavB8Kg25S7g603ArkO7m7ZMPI9YWYdnL0OTwKYrwYZX4UPf312XL0fjAqjgbqOMPNzIQVGHWCOdhs8PPkgeZvgA3nQbeBJ6GiPdWj8+s3c+tVYINShMTPq0NbBxpc1aimAtnbW2ga+/GwEw6BLHWCkdhk8zAgdrfH3YDMqpcaU0NEg0MOHWaHDF3Vw9piROzAbFb68HWzMeo8YHToaeFIHf4dzydx1JeGjY2p3wcPs0NHSEQ2+GHS9OUV7S9/wzPiG35Qng6ZZKx0j6+Du/5k/Blh3BxkjtTTYtKUmjf//Wntd/hxsG7j6XHhbg4ru4R4/x8w6tBTKA+ELCszTroKHsw+RGSsUT8KHURq+1be0lcPo0BFoA64zZtRBMvfbvjsDbtP3Rkt18Md7QjJ2wHUVvvy9jmgpfPjic9LW94Pkv3Ul4aP96mzGQlavXq2nn35aVqtVo0aN0qpVqzR27FiP59PwAej+r/s/vuvqWUxb+4Zj9Lf6hg+Us4u6NXygNv0jzP5hcnbobMNjrYWVpnUYJfdq0Pi5RnFVh8Y1kOS0Do1XNE1r4GyF1bQOnmhLHaLOV7s12N0U36nVOkgtr2DdqYNU/3ps+/ZKcv4+OP9Wkc8HFk/qIDm+J5zVwRccQliTx1r7nHhaB0+3MLj72Wis8XujtcPto85X2/92VoeG1y+1vK7wVx0k99YTHZ2n4+muXbuUmZmpo0ePKiEhQY8++qhmz57t0Gbjxo167LHH9MUXX+h73/uennrqKd1+++2GvYYQm81mM2zukjZs2KBZs2ZpzZo1SkpK0sqVK7Vx40YVFhaqd+/eLT63srJSUVFRKj/5Z1m6O3/jNaxsGzNqcPX0A9bSlWWdrWQaPnTOBpfGK5WmnNVACr46uDPIuqqDqxo0MKIW3mzadlWLTf8IazVwNq5HS++HxprWxdd18KYGkvtXXXbGVfjy5DPS1jp4+7qdcacWzj4nDXXw9r0gta0OZtag6fqy8Wel8fuhsvKi+vWaq4qKClksFp/1r7GGcenUVy/LYunahvl43ldPx9OTJ09qxIgRmj9/vubOnau8vDwtXLhQ27dv16RJkyRJn3zyiW666SYtW7ZM//7v/67c3Fw99dRTOnjwoEaMGOH162uJ4cEjKSlJN954o55//nlJUl1dnRISEvTAAw9oyZIlDm2rq6tVXX31Q1RZWamEhIQWg4c/tPUD5+pD1tJmSHdXLmYyog4drQZS8zq09vNEoNXBVwOQOwOwv2vjy8G2JS3VwtPA4Wtm1UDyfF0ZjMGjpKTEoa/h4eEKD3deY0/GU0lavHixtm/friNHjtinzZgxQ+Xl5dqxY4ck6Z577lFVVZW2bdtmbzNu3DiNHj1aa9as8fr1tcTQn1pqampUUFCgrKws+7TQ0FClpqYqPz+/Wftly5bpiSeeaDa9oluYbCa+2Ztq+gFvuO/tB9CbHUZdMWsl4Gwl5+6mdld8VYdgroHkfh1aGmiCvQaSb94PZg6KRvI2gLeX19/Al+vKQJWQkOBwPzs7W0uXLm3WztPxVJLy8/OVmprqMG3SpElauHChQ5vMzMxmbbZs2eLpS3GbocHj66+/Vm1trWJiYhymx8TE6Pjx483aZ2VlORSgYYuHK+58KzCSP795NqxgWvuW6MvB3dWg4y/u1qBBW2sRiDWQXA823mxZc2dZ/qyBL3/eMmIdEQifB1c83cLoLX/WIOSCedfNirpQI0uI9/Vr6KuzLR7OeDqeSpLVanXavrKyUpcuXVJkZKTLNlar1ePX5C5Tdi51V0ubmBpr+AC19LufLz9krla23mrLb61NB9uWatC4XWOB8i2iLb+7exo6nLX1pg6+fi9I5tSh4X0y7fs1ba5DwzKNrkNTIUnj3N660vQ1Ovuc+PqzYfQ6QvJ8nwzH9UTz66aYGcq8Yfb+a/5gsVgM+1koUBkaPHr27KlOnTqprKzMYXpZWZliY2M9mlfjD0jDSqTpns6bnDyvpTBixoerpZVpwxE50tU9z119oFwFjoYaNOz93dJRAg21aFwHT2rQlkHXVR2c1aCBs1o4G2g9PTLCWR0k92thVB0ajjRwZ2XbePBtbZB19Zg/6yC1HjRctY/6Vx2aBpCW6tD06ClXOzV7+9loC3fqYD9671/3Wxt4Xa0nGmu6vgz0OniyvmyPvBlPY2Njnba3WCyKjIxssY2nY7QnDA0eYWFhSkxMVF5enqZOnSqpfmeYvLw8ZWRkeDXPlj5ITQfhxu2dHTGw50ytzz9U7gYNyfEid/ZD3RoNQJ58qJrWw9khms6OnGhYwRixcnEnbDitgeoP92v8fFe18OZwTFfvCSPeD5J7dXB2X3I87NG2b2+zOrgTwFy9N1qqg+T794S7dWhJw/uita0frkJH4/u+rIMnAczbOvzPrrCrhwT/a5qzz4WrraFNBWodJFfryubrio4UQLwZT5OTk/XOO+84TNu5c6eSk5Md2jQc7eKqja8ZfgKxzMxM/dd//ZdeffVVHTt2TPfff7+qqqo0Z84cw5bpNN27+BC25bC+ptz9IP3PrrBmV9Z150q7nnB13LtZdXBWi/NvFbkMHc7a+sqZHZFuvyd8WQfJ+XvCWR1c1aKtdXDn/Ae+PpeGM+7UwWzNtpj6cR3RUh1aen/4grvnyDCrDi19QevoWhtPs7KyNGvWLHv7+fPn6//+7//08MMP6/jx4/rzn/+sN954Qw8++KC9zW9+8xvt2LFDzzzzjI4fP66lS5fqwIEDXm8ccIfhweOee+7R8uXL9fjjj2v06NE6fPiwduzY0WxnFl8zO3x4Ejp8MU93eDLgSsbWwZ2ViRErmNZWqkaGD1eDrStmrWDNDmGe1sEb3v70Zmb48DR0eBI4fHk0S0v1M7sOhA5HrY2npaWlKi4utrcfMGCAtm/frp07d2rUqFF65pln9PLLL9vP4SFJ48ePV25url566SWNGjVKf/3rX7VlyxbDzuEhmXTK9IyMDJ06dUrV1dXat2+fkpKSzFisaYOuJ5sM/cGTOrSFu0HJXwOsJ1uB2rqCdXewdacWRryHzAofnoQOfw0yZoQPTwdbs2vhbg0kc+uA5loaT3NycrRr1y6H9ikpKTp06JCqq6tVVFTU7KylkjR9+nQVFhaqurpaR44cMfSspVI7u1aLM0YPut7u02E2oweaQA9fDTz9CcobbQkd3r5HnPW/ta09/vj5qbXQYdZPjoEg0AZbZ+HDjHVloNUBxmv3wUMKjBVsIHyQOlJRrbt0AAAcX0lEQVQdWhpwjPxNO9C2+rTGyIG5aS3c3dLhj9oYudXDl1u/jGb2OiJQ6wBjBW3w8PTKhUZ8oNqyP4O/dIQ6uDOYmrmCDeT3g+TeoOtpHdoawAI5fHgimEJHAyO2EgdjHWCcoA0e3jBjXwdvBxlXl7M3QiDXoa08+Qbv6/DhzsrVnTr4olZt3ZJhdB2k1l9noIYPs+vgD+68fzpCHWCMDhU8fMlXg0wgc2fFEux18NVPDWYcuWEkI39yCfZBxlehnDrUC/Y6oO06XPDwZZJvzF+DjKc/OTUI5J3u/M2o81oE28rVmzq09RBwoDVmbh2GMYIueDg7A2lbmXECpWDgizoE07d8IwVjHcwKo4EewHxRB3cCmKd1MHvA5csJjBJ0wSNYBPrK1SzBUAczVrDBUAcjBGMAM0J7qUPTLyetbR1258imjvrZ6Mg6ZPDw9UDjj5WKL7b88I2m/Wj6fvD2JzijBesg4+mAC8C1Dhk8gkWwXfgoWAOY1PrRDP4caBpfKM5ogVwHbwTqPlBmBDC+nCBQETz+xZP9GwJtB7q2fLtty7kLAq0OUmB8029rAGuvO88F69aOQMB7ojkzAzl8i+BhgPa0gm3LN9z2VIeWtBbAOkodmiKA1fP1lkB/D7je7oTeXvZzQdsRPBAw2stA01EZvQXM3QHXiCPfJN8d/eaPIBoIWwKBBkEdPHz5E4O32tu3GSAY3RTfyd9dAOCmoA4e8A12IKtHHQDAeAQPFzrK3vu+wu+3/mHGN31OsAcjdNR9n0DwCAjs29Bcaz85sWndOGz5qWdEHbwZbP29fgik94O/awHfIHgEGQZcBKOO9O02EA8zBwIJwcPHOtIKtr3z5icGf16Z16ijOQDAlzp08AikTYiBxBe/6RPAAADOdOjg0RQ70QEAYCyCBwAAMA3BAwBgKn6K7diCJnhwNEfH5elVev19ThNwXhcArgVN8HCFQQbtGafQR7DisGK4EvTBA4BzhHLfYwd0oO0IHgAAj/gjgHHW0vaD4AEABmFfF6A5ggcAADANwaMd4Td9AIGILT9ojOABSZw+HgBgDoIHECQ8PZ8JAAQiggeAdo2fII2150ytv7uAIEPwAAAApiF4AAg4nLMBaL8IHgAAtCPnzp3TzJkzZbFY1KNHD6Wnp+vChQsu21+5ckWLFy/W9ddfr2uuuUbx8fGaNWuWzpw549AuJSVFISEhDrf58+d73D+CBwCgRRz1Flxmzpypo0ePaufOndq2bZv27Nmj++67z2X7ixcv6uDBg3rsscd08OBBvfnmmyosLNRdd93VrO28efNUWlpqv/3xj3/0uH+dPX4GAAAISMeOHdOOHTv06aefasyYMZKkVatW6fbbb9fy5csVHx/f7DlRUVHauXOnw7Tnn39eY8eOVXFxsfr27Wuf3rVrV8XGxrapjx1+iwdJHgDgL5WVlQ636urqNs0vPz9fPXr0sIcOSUpNTVVoaKj27dvn9nwqKioUEhKiHj16OExfv369evbsqREjRigrK0sXL170uI9s8QAAwEO2A5/K1tX7i+XZLtbvQJ2QkOAwPTs7W0uXLvV6vlarVb1793aY1rlzZ0VHR8tqtbo1j8uXL2vx4sW69957ZbFY7NN//vOfq1+/foqPj9dnn32mxYsXq7CwUG+++aZHfSR4AADgJyUlJQ6De3h4uNN2S5Ys0VNPPdXivI4dO9bm/ly5ckU/+9nPZLPZ9MILLzg81ng/keuvv17x8fG6+eabVVRUpEGDBrm9jKAMHtO+X+OXyzID8D/O4Ir2xGKxOAQPVxYtWqTZs2e32GbgwIGKjY3V2bNnHaZ/9913OnfuXKv7ZjSEjlOnTumDDz5otV9jx46VJJ04caL9Bw+gveL8FQCc6dWrl3r16tVqu+TkZJWXl6ugoECJiYmSpA8++EB1dXVKSkpy+byG0PHPf/5TH374oa677rpWl3X48GFJUlxcnJuvol6H37kUAID2YtiwYZo8ebLmzZun/fv36+OPP1ZGRoZmzJjhcETL0KFDtXnzZkn1oeOnP/2pDhw4oPXr16u2tlZWq1VWq1U1NfVfhoqKivTkk0+qoKBAX3zxhd566y3NmjVLN910k0aOHOlRH9niAQBAO7J+/XplZGRo4sSJCg0N1bRp0/SnP/3JoU1hYaEqKiokSadPn9Zbb70lSRo9erRDuw8//FApKSkKCwvT+++/r5UrV6qqqkoJCQmaNm2aHn30UY/7R/AAAKAdiY6OVm5ubottbDab/e/+/fs73HcmISFBu3fv9kn/+KkFQW/a99kvAgCCBcEDAACYhuABAABMQ/AAAACmMSx4/O53v9P48ePVtWvXZud6BwAAHZNhwaOmpkbTp0/X/fffb9QiAABAkDHscNonnnhCkpSTk2PUIgAAQJAJqPN4VFdXO1wSuLKy0o+9AQAAvhZQO5cuW7ZMUVFR9lvTywUDAIDg5lHwWLJkiUJCQlq8HT9+3OvOZGVlqaKiwn4rKSnxel6AN87siPR3FwA7LhqI9sijn1rcvSyvt8LDwxUeHu718zuaad+v0aZ/hPm7Gzr/VpG/uwAACBIeBQ93L8sLAADgjGE7lxYXF+vcuXMqLi5WbW2tDh8+LEkaPHiwunXrZtRiAQBAADMseDz++ON69dVX7fdvuOEGSVcvsQsAADoew45qycnJkc1ma3YjdAAA0HEF1OG0AACgfSN4AAAA0xA8AACAaQgeAADANAQPAABgGoIHAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDQEDwAA2pFz585p5syZslgs6tGjh9LT03XhwoUWnzN79myFhIQ43CZPnuzQ5vLly1qwYIGuu+46devWTdOmTVNZWZnH/SN4AADQjsycOVNHjx7Vzp07tW3bNu3Zs0f33Xdfq8+bPHmySktL7be//OUvDo8/+OCDevvtt7Vx40bt3r1bZ86c0d133+1x/zp7/AwAABCQjh07ph07dujTTz/VmDFjJEmrVq3S7bffruXLlys+Pt7lc8PDwxUbG+v0sYqKCr3yyivKzc3VzTffLElat26dhg0bpr1792rcuHFu95EtHgAA+EllZaXDrbq6uk3zy8/PV48ePeyhQ5JSU1MVGhqqffv2tfjcXbt2qXfv3hoyZIjuv/9+ffPNN/bHCgoKdOXKFaWmptqnDR06VH379lV+fr5HfWSLBwAAHjr/zkmFhHk/hJ6v+U6SlJCQ4DA9OztbS5cu9Xq+VqtVvXv3dpjWuXNnRUdHy2q1unze5MmTdffdd2vAgAEqKirSI488ottuu035+fnq1KmTrFarwsLC1KNHD4fnxcTEtDhfZwgeAAD4SUlJiSwWi/1+eHi403ZLlizRU0891eK8jh075nU/ZsyYYf/7+uuv18iRIzVo0CDt2rVLEydO9Hq+zhA8AADwE4vF4hA8XFm0aJFmz57dYpuBAwcqNjZWZ8+edZj+3Xff6dy5cy7333A1r549e+rEiROaOHGiYmNjVVNTo/LycoetHmVlZR7NVyJ4AAAQ8Hr16qVevXq12i45OVnl5eUqKChQYmKiJOmDDz5QXV2dkpKS3F7el19+qW+++UZxcXGSpMTERHXp0kV5eXmaNm2aJKmwsFDFxcVKTk726LWwcykAAO3EsGHDNHnyZM2bN0/79+/Xxx9/rIyMDM2YMcPhiJahQ4dq8+bNkqQLFy7ooYce0t69e/XFF18oLy9PU6ZM0eDBgzVp0iRJUlRUlNLT05WZmakPP/xQBQUFmjNnjpKTkz06okViiwcAAO3K+vXrlZGRoYkTJyo0NFTTpk3Tn/70J4c2hYWFqqiokCR16tRJn332mV599VWVl5crPj5et956q5588kmHfU5WrFhhn191dbUmTZqkP//5zx73j+ABAEA7Eh0drdzc3Bbb2Gw2+9+RkZF67733Wp1vRESEVq9erdWrV7epf/zUAgAATEPwAAAApiF4AAAA0xA8AACAaQgeAADANAQPAABgGoIHAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkMCR5ffPGF0tPTNWDAAEVGRmrQoEHKzs5WTU2NEYsDAABBorMRMz1+/Ljq6ur04osvavDgwTpy5IjmzZunqqoqLV++3IhFAgCAIGBI8Jg8ebImT55svz9w4EAVFhbqhRdeIHgAANCBGRI8nKmoqFB0dHSLbaqrq1VdXW2/X1lZaXS3AACAiUzZufTEiRNatWqVfvWrX7XYbtmyZYqKirLfEhISzOgeAAAwiUfBY8mSJQoJCWnxdvz4cYfnnD59WpMnT9b06dM1b968FueflZWliooK+62kpMTzVwQAAAKWRz+1LFq0SLNnz26xzcCBA+1/nzlzRhMmTND48eP10ksvtTr/8PBwhYeHe9IlAAAQRDwKHr169VKvXr3canv69GlNmDBBiYmJWrdunUJDOWUIAAAdnSE7l54+fVopKSnq16+fli9frq+++sr+WGxsrBGLBAAAQcCQ4LFz506dOHFCJ06cUJ8+fRwes9lsRiwSAAAEAUN+/5g9e7ZsNpvTGwAA6LjY8QIAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAaEfOnTunmTNnymKxqEePHkpPT9eFCxdafI6rs5E//fTT9jYpKSnNHp8/f77H/TPtInEAAMB4M2fOVGlpqXbu3KkrV65ozpw5uu+++5Sbm+vyOaWlpQ733333XaWnp2vatGkO0+fNm6ff/va39vtdu3b1uH8EDwAA2oljx45px44d+vTTTzVmzBhJ0qpVq3T77bdr+fLlio+Pd/q8pif33Lp1qyZMmOBwGRSpPmi09USg/NQCAICfVFZWOtyqq6vbNL/8/Hz16NHDHjokKTU1VaGhodq3b59b8ygrK9P27duVnp7e7LH169erZ8+eGjFihLKysnTx4kWP+8gWDwAA/CQhIcHhfnZ2tpYuXer1/KxWq3r37u0wrXPnzoqOjpbVanVrHq+++qq6d++uu+++22H6z3/+c/Xr10/x8fH67LPPtHjxYhUWFurNN9/0qI8EDwAAPHTk7110TacuXj+/qjZEklRSUiKLxWKf7uoK7UuWLNFTTz3V4jyPHTvmdX8aW7t2rWbOnKmIiAiH6ffdd5/97+uvv17x8fG6+eabVVRUpEGDBrk9f4IHAAB+YrFYHIKHK4sWLdLs2bNbbDNw4EDFxsbq7NmzDtO/++47nTt3zq19M/7+97+rsLBQGzZsaLXt2LFjJUknTpwgeAAA0J706tVLvXr1arVdcnKyysvLVVBQoMTEREnSBx98oLq6OiUlJbX6/FdeeUWJiYkaNWpUq20PHz4sSYqLi2u1bWPsXAoAQDsxbNgwTZ48WfPmzdP+/fv18ccfKyMjQzNmzHA4omXo0KHavHmzw3MrKyu1ceNGzZ07t9l8i4qK9OSTT6qgoEBffPGF3nrrLc2aNUs33XSTRo4c6VEfCR4AALQj69ev19ChQzVx4kTdfvvt+vGPf6yXXnrJoU1hYaEqKiocpr3++uuy2Wy69957m80zLCxM77//vm699VYNHTpUixYt0rRp0/T222973D9+agEAoB2Jjo5u8WRhkmSz2ZpNu++++xx2IG0sISFBu3fv9kn/2OIBAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkIHgAAwDQEDwAAYBqCBwAAMA3BAwAAmIbgAQAATEPwAAAApiF4AAAA0xA8AACAaQgeAADANAQPAABgGoIHAAAwDcEDAACYhuABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKYheAAAANMQPAAAgGkMCx533XWX+vbtq4iICMXFxekXv/iFzpw5Y9TiAACApN/97ncaP368unbtqh49erj1HJvNpscff1xxcXGKjIxUamqq/vnPfzq0uXz5shYsWKDrrrtO3bp107Rp01RWVuZx/wwLHhMmTNAbb7yhwsJCbdq0SUVFRfrpT39q1OIAAICkmpoaTZ8+Xffff7/bz/njH/+oP/3pT1qzZo327duna665RpMmTdLly5ftbR588EG9/fbb2rhxo3bv3q0zZ87o7rvv9rh/nT1+hpsefPBB+9/9+vXTkiVLNHXqVF25ckVdunRx+pzq6mpVV1fb71dUVEiSzp+/JEm6eL7O/lhN1Xf2v7+rtrWprzVVl+x/Xzx/xf53ZeXVXBZyocb+t+3i1b/P11zthyRV1YZ4vPzKRvNoPO+Q81f7VWmrbdRH53WQ2lYLb+vQtAZSx6yD1Pb3Q2WT5/urDo1rUL8M/9ah8fz9WYf65dTXwt91kK7Wwp06+HI9Wb+c5u8Js9aTTedvr8O//rXZ2vZa3VFV1/z/xpvnV1ZWOkwPDw9XeHh4m+b9xBNPSJJycnLcam+z2bRy5Uo9+uijmjJliiTptddeU0xMjLZs2aIZM2aooqJCr7zyinJzc3XzzTdLktatW6dhw4Zp7969GjdunPsdtJngm2++sf3sZz+z/ehHP2qxXXZ2tk0SN27cuHHj5vWtqKjIsPHs0qVLttjYWJ/0s1u3bs2mZWdn+6yv69ats0VFRbXarqioyCbJdujQIYfpN910k+3Xv/61zWaz2fLy8mySbN9++61Dm759+9qeffZZj/pl2BYPSVq8eLGef/55Xbx4UePGjdO2bdtabJ+VlaXMzEz7/fLycvXr10/FxcWKiooysqsBrbKyUgkJCSopKZHFYvF3d/yGOlxFLepRh3rUoV5FRYX69u2r6Ohow5YRERGhkydPqqampvXGrbDZbAoJcdz609atHd6wWq2SpJiYGIfpMTEx9sesVqvCwsKa7TPSuI27PAoeS5Ys0VNPPdVim2PHjmno0KGSpIceekjp6ek6deqUnnjiCc2aNUvbtm1rVugGrjYxRUVFdegPUwOLxUIdRB0aoxb1qEM96lAvNNTYAzYjIiIUERFh6DKa8nT8DWQeBY9FixZp9uzZLbYZOHCg/e+ePXuqZ8+e+v73v69hw4YpISFBe/fuVXJysledBQCgI/J0/PVEbGysJKmsrExxcXH26WVlZRo9erS9TU1NjcrLyx22epSVldmf7y6PgkevXr3Uq1cvjxbQoK6ufoenxjuPAgCA1rVl/G3NgAEDFBsbq7y8PHvQqKys1L59++xHxiQmJqpLly7Ky8vTtGnTJEmFhYUqLi72eGNCp6VLly716SuQtG/fPm3ZskVhYWGqrq7WwYMHtWDBAoWFhWnZsmXq3Nn9vNOpUyelpKR49Jz2iDrUow5XUYt61KEedahHHaTi4mKdPHlS+/fv10cffaTbb79dVqtV3bp1U1hYmCRp6NCh+rd/+zcNGzZMISEhqq2t1e9//3sNHz5cNTU1+vWvf62LFy9q1apV6ty5syIiInTmzBk9//zzGj16tM6dO6df/epXSkhIUHZ2tmcd9GhXVDd99tlntgkTJtiio6Nt4eHhtv79+9vmz59v+/LLL41YHAAA+Je0tDSnR9F8+OGH9jaSbOvWrbPfr6ursz322GO2mJgYW3h4uG3ixIm2wsJCh/leunTJ9p//+Z+2a6+91ta1a1fbf/zHf9hKS0s97l/IvzoAAABgOK7VAgAATEPwAAAApiF4AAAA0xA8AACAaYImeNx1113q27evIiIiFBcXp1/84hc6c+aMv7tlui+++ELp6ekaMGCAIiMjNWjQIGVnZ/vk9L3BxptLP7cHq1evVv/+/RUREaGkpCTt37/f310y3Z49e3TnnXcqPj5eISEh2rJli7+7ZLply5bpxhtvVPfu3dW7d29NnTpVhYWF/u6WX7zwwgsaOXKk/cytycnJevfdd/3dLbgQNMFjwoQJeuONN1RYWKhNmzapqKhIP/3pT/3dLdMdP35cdXV1evHFF3X06FGtWLFCa9as0SOPPOLvrpnOm0s/B7sNGzYoMzNT2dnZOnjwoEaNGqVJkybp7Nmz/u6aqaqqqjRq1CitXr3a313xm927d2vBggXau3evdu7cqStXrujWW29VVVWVv7tmuj59+ugPf/iDCgoKdODAAd18882aMmWKjh496u+uwRnvjhL2v61bt9pCQkJsNTU1/u6K3/3xj3+0DRgwwN/d8Bt3r8DYHowdO9a2YMEC+/3a2lpbfHy8bdmyZX7slX9Jsm3evNnf3fC7s2fP2iTZdu/e7e+uBIRrr73W9vLLL/u7G3AiaLZ4NHbu3DmtX79e48ePV5cuXfzdHb+rqKgw9GqMCAw1NTUqKChQamqqfVpoaKhSU1OVn5/vx54hEFRUVEhSh18X1NbW6vXXX1dVVRXXBQtQQRU8Fi9erGuuuUbXXXediouLtXXrVn93ye9OnDihVatW6Ve/+pW/uwKDff3116qtrW3x0tXomOrq6rRw4UL96Ec/0ogRI/zdHb/4/PPP1a1bN4WHh2v+/PnavHmzhg8f7u9uwQm/Bo8lS5YoJCSkxdvx48ft7R966CEdOnRIf/vb39SpUyfNmjVLtnZy4lVPayFJp0+f1uTJkzV9+nTNmzfPTz33LW/qAHR0CxYs0JEjR/T666/7uyt+M2TIEB0+fNh+YbO0tDT97//+r7+7BSf8esr0r776St98802LbQYOHGi/qE1jX375pRISEvTJJ5+0i81pntbizJkzSklJ0bhx45STk6PQ0KDaeOWSN++JnJwcLVy4UOXl5UZ3z69qamrUtWtX/fWvf9XUqVPt09PS0lReXt5htwCGhIRo8+bNDjXpSDIyMrR161bt2bNHAwYM8Hd3AkZqaqoGDRqkF1980d9dQRN+vXxfWy7zW1dXJ0mqrq72ZZf8xpNanD59WhMmTFBiYqLWrVvXbkKHZOyln4NdWFiYEhMTlZeXZx9k6+rqlJeXp4yMDD/3Dmaz2Wx64IEHtHnzZu3atYvQ0URdXV27GR/am6C4bvC+ffv06aef6sc//rGuvfZaFRUV6bHHHtOgQYPaxdYOT5w+fVopKSnq16+fli9frq+++sr+WGxsrB97Zr7i4mKdO3dOxcXFqq2t1eHDhyVJgwcPVrdu3fzcO2NkZmYqLS1NY8aM0dixY7Vy5UpVVVVpzpw5/u6aqS5cuKATJ07Y7588eVKHDx9WdHS0+vbt68eemWfBggXKzc3V1q1b1b17d/t+PlFRUYqMjPRz78yVlZWl2267TX379tX58+eVm5urXbt26b333vN31+CMfw+qcc9nn31mmzBhgi06OtoWHh5u69+/v23+/Pm2L7/80t9dM926deucXu44SP4rfcqdSz+3R6tWrbL17dvXFhYWZhs7dqxt7969/u6S6T788EOn//dpaWn+7pppXK0HGl/qvKP45S9/aevXr58tLCzM1qtXL9vEiRNtf/vb3/zdLbjg1308AABAx9J+dg4AAAABj+ABAABMQ/AAAACmIXgAAADTEDwAAIBpCB4AAMA0BA8AAGAaggcAADANwQMAAJiG4AEAAExD8AAAAKb5/1NdmcG9iFccAAAAAElFTkSuQmCC",
      "text/plain": [
       "PyPlot.Figure(PyObject <matplotlib.figure.Figure object at 0x32d19cbd0>)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "PyPlot.Figure(PyObject <matplotlib.figure.Figure object at 0x32ce0f2d0>)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# plot showing zeros of Delta\n",
    "D=lambdify(simplify(Delta(beta,L,k)),[k]);\n",
    "kR = linspace(-3,3,50); kI = linspace(-3,3,50);\n",
    "zarg = [sin(angle(D(xx+im*yy))) for yy in kI, xx in kR]\n",
    "DZ=contourf(kR, kI, zarg, cmap=get_cmap(\"Spectral\"))\n",
    "cbar = colorbar(DZ)\n",
    "figure()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As shown above, all the zeros of $\\Delta$ are on the real line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  6.498490 seconds (10.38 M allocations: 413.047 MB, 4.44% gc time)\n",
      " "
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "WARNING: using Roots.D in module Main conflicts with an existing identifier.\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " 0.137843 seconds (59.94 k allocations: 2.689 MB)\n",
      "Had to throw away 14 (of 400) roots!\n"
     ]
    }
   ],
   "source": [
    "using Roots\n",
    "rd(w)=real(N(D(w)));\n",
    "@time nkrts=Roots.fzeros(rd, -rts_btwn,rts_btwn); #gives me the roots between -rts_btwn and rts_btwn\n",
    "\n",
    "tol=10e-13; #Find roots to this tolerance\n",
    "\n",
    "#make sure we're actually getting roots\n",
    "@time norms=map(norm,map(D,nkrts))\n",
    "nrts=nkrts[norms.<tol]\n",
    "bdrts=nkrts[norms.>tol]\n",
    "\n",
    "mypolecount=length(nrts);\n",
    "nofound=length(nkrts);\n",
    "badno=length(bdrts);\n",
    "\n",
    "println(\"Had to throw away $badno (of $nofound) roots!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  8.384838 seconds (396.41 k allocations: 16.110 MB, 0.09% gc time)\n",
      "Added back 42 roots!\n"
     ]
    }
   ],
   "source": [
    "#make sure we're actually getting roots (round 2)...using a slower but more accurate way to check if nkrt is a root\n",
    "@time norms2=map(norm,N(map(Delta(beta,L,k),bdrts)));\n",
    "nrts=append!(bdrts[norms2.<tol],nrts)\n",
    "BADrts=bdrts[norms2.>tol];\n",
    "\n",
    "mypolecount=length(nrts);\n",
    "back=length(bdrts)-length(BADrts);\n",
    "\n",
    "println(\"Added back $back roots!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this would not work if the roots weren't real"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is strange because the elements of nkrts are all really roots to within $10^{-14}$ but when they are plugged in we are off by more digits.  Where are we losing accuracy??"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "BADrts\n",
    "\n",
    "D(BADrts[1])\n",
    "\n",
    "N(Delta(beta,L,BADrts[1]))\n",
    "\n",
    "#These should both be 0!! Where am I losing accuray?\n",
    "\n",
    "D(-195.875) #Julian object evaluated at 199.875\n",
    "\n",
    "BADrts[1]\n",
    "\n",
    "N(Delta(beta,L,-195.87499999999997)) #SymPy object evalated at 199.875 then N takes to Julian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 0.5.1",
   "language": "julia",
   "name": "julia-0.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "0.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
