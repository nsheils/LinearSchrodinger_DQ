# Prepares to plot u(t,x) for 1D pseudoperiodic problem

# Indicate which experiments to run
################
# ExpList = (true,true,true,true,true,true,true,true);
ExpList = (true,false,false,false,false,false,false,false);

using PyPlot;

# Functions
################

function PSQS(TS)
    myeps=.1/L;
    # print(TS)
    # print(length(TS))
    ps=Array{Int64}(undef,length(TS));
    qs=Array{Int64}(undef,length(TS));
    maxq=10;
    maxp=ceil((4*pi*tmax*maxq)/L^2);
    for j in 1:length(TS)
        t=TS[j]
        for q in 1:maxq, p in 0:maxp
            ps[j]=p
            qs[j]=q
            if 4*pi*t/L^2-myeps<p/q<4*pi*t/L^2+myeps
                break
            elseif j==1
                ps[j]=0
                qs[j]=1/myeps
            else
                ps[j]=ps[j-1]
                qs[j]=qs[j-1]
            end
        end
    end
    #the following should be not too big relative to length(ts).  If not, increase maxv.
    # if sum(1-(abs.(L^2*ps./(4*pi*qs)-TS).<myeps))>1
    #     print("ERROR: increase maxv")
    # end
    return ps,qs
end

# Run experiments
################
# Define master directory
outputsdir = string(@__DIR__,"/outputs");
ispath(outputsdir) ? println("Main output directory exists") : mkdir(outputsdir)
# Experiments 1, 2
IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, IDslope, PoleCnt = 1.0, 0.2, 5.0, 0.25, 0.5, 0, 10000;
include("1d-Pseudoperiodic/setup1234.jl");
if ExpList[1] ##Generates frames necessary for Fig 2
    ispath(string(outputsdir,"/Experiment1")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment1"))
    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1.05,1.05]);
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment1/LS_irr_$j.pdf"));
        clf();
    end;
end;
if ExpList[2] ##Generates frames necessary for Fig 3
    ispath(string(outputsdir,"/Experiment2")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment2"))
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1.05,1.05]);
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment2/LS_r_$j.pdf"))
        clf()
    end
end

# Experiments 3, 4
IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, IDslope, PoleCnt = 1.0, 0.2, 2.0, 0.25, 0.5, 0, 10000;
include("1d-Pseudoperiodic/setup1234.jl")
if ExpList[3] ##Generates frames necessary for Fig 4
    ispath(string(outputsdir,"/Experiment3")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment3"))
    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1.05,1.05]);
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment3/LS_irr_$j.pdf"))
        clf()
    end
end
if ExpList[4]  ##Generates frames necessary for Fig 5
    ispath(string(outputsdir,"/Experiment4")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment4"))
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1.05,1.05]);
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment4/LS_r_$j.pdf"))
        clf()
    end
end

# Experiment 5
if ExpList[5] ##Generates frames necessary for Fig 5
    IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, TimeNumerator, TimeDenominator, PoleCnt = 1.0, 0.2, 2.0, 0.25, 0.2, 1, 1, 10000;
    include("1d-Pseudoperiodic/setup5.jl")
    ispath(string(outputsdir,"/Experiment5")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment5"))
    for j=1:length(rs)
        fig=figure("LS_rational_time_u0slope",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1,2]);
        r=rs[j];
        U0=u0.(X,c,r);
        plot(X,U0,"r",label=latexstring("\$u_0(x)\$"))
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(\\frac{$p}{$q} \\frac{L^2}{4\\pi},x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(\\frac{$p}{$q} \\frac{L^2}{4\\pi},x))\$")
        mytitle=latexstring("r=","$r")
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment5/LS_r_u0slope_$j.pdf"))
        clf()
    end
    ##Generates frames necessary for Fig 6
    IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, TimePassed, PoleCnt = 1.0, 0.2, 2.0, 0.25, 0.2, round(10^3*1*1.0^2/(13.0*1))*10^(-3), 10000;
    include("1d-Pseudoperiodic/setup5irr.jl")
    for j=1:length(rs)
        fig=figure("LS_irrational_time_u0slope",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-1,2]);
        r=rs[j];
        U0=u0.(X,c,r);
        plot(X,U0,"r",label=latexstring("\$u_0(x)\$"))
        U = u(X,T,c,r)
        TPrint=string(T)[1:5];
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u($TPrint,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u($TPrint,x))\$")
        mytitle=latexstring("r=","$r")
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment5/LS_irr_u0slope_$j.pdf"))
        clf()
    end
end

# Experiment 6
if ExpList[6] ##Generates frames necessary for Fig 7
    IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, IDslope, PoleCnt = 1.0, 0.2, 2.0, 0.02, 0.125, 8, 10000;
    include("1d-Pseudoperiodic/setup1234.jl")
    ispath(string(outputsdir,"/Experiment6")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment6"))
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-.75,1.1]);
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment6/LS_r_$j.pdf"))
        clf()
    end
    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-.75,1.1]);
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment6/LS_irr_$j.pdf"));
        clf();
    end;
end

# Experiment 7
if ExpList[7] ##Generates frames necessary for Fig 8
    IntervalLength, BCbeta0, BCbeta1, IDwidth, IDcenter, IDslope, PoleCnt = 1.0, 0.2, 2.0, 0.02, 0.125, 8, 10000;
    include("1d-Pseudoperiodic/setup1234.jl")
    ispath(string(outputsdir,"/Experiment7")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment7"))
    ps = [1,1,1,2,2,2];
    qs = [1,2,3,1,2,3];
    for j=1:length(ps)
        fig=figure("LS_rational_time",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-.75,1.1]);
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment7/p$(p)_q$q.pdf"))
        clf()
    end
end


# Experiment 8
if ExpList[8]  ##Generates frames necessary for Fig 9
    IntervalLength, BCbeta0, BCbeta1, IDwidth, IDslope, TimeNumerator, TimeDenominator, PoleCnt = 1.0, 0.2, 2.0, 0.02, 8, 2, 1, 10000;
    include("1d-Pseudoperiodic/setup8.jl")
    ispath(string(outputsdir,"/Experiment8")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment8"))
    for j=1:length(cs)
        fig=figure("LS_rational_time_u0trans",figsize=(5,3))
        ax=gca();
        ax.set_xlim([0,L])
        ax.set_ylim([-.75,r*w/2+1]);
        c=cs[j];
        U0=u0.(X,c,r);
        plot(X,U0,"r",label=latexstring("\$u_0(x)\$"))
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(\\frac{$p}{$q} \\frac{L^2}{4\\pi},x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(\\frac{$p}{$q} \\frac{L^2}{4\\pi},x))\$")
        mytitle=latexstring("c=","$c")
        title(mytitle)
        xlabel(L"x")
        legend(loc="lower left")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment8/LS_r_u0trans_$j.pdf"))
        clf()
    end
end
