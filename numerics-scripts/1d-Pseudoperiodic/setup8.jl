# Load ARGS
################
try
    IntervalLength = parse(Float64,ARGS[1]);
    BCbeta0 = parse(Float64,ARGS[2]);
    BCbeta1 = parse(Float64,ARGS[3]);
    IDwidth = parse(Float64,ARGS[4]);
    IDslope= parse(Float64,ARGS[5]);
    TimeNumerator  = parse(Float64,ARGS[6]);
    TimeDenominator= parse(Float64,ARGS[7]);
    PoleCnt = parse(Int64,ARGS[8]);
catch
    # print information on the error and quit gracefully.
    print("If you want to manually specify the parameters, then call this script directly, passing through ARGS 8 arguments, the first 7 parsable as Float64, and the seventh parsable as Int64.\nWill try to use already defined arguments instead.\n")
end

# Parameters of problem
################
# Length of interval
L = IntervalLength;
# Coupling coeficients
(beta0,beta1) = (BCbeta0,BCbeta1);
# Check the problem is well-posed
# Not sure how to get this to throw an error on fail
if imag(acos((1+beta0*beta1)/(beta0+beta1)))!=0
    println("ERROR: The boundary conditions chosen are inadmissable, this problem is ill-posed")
end
# Initial datum
################

# Parameters
(w,r) = (IDwidth,IDslope)
# Check the parameters make sense
# not implemented

# Initial datum
function u0(x,c)
    if x>c-w/2 && x<c+w/2
        return (x-c)*r+1
    else
        return 0
    end
end;

function u0(x,c,r)
    return u0(x,c)
end

# Fourier transform of initial datum
function u0hat(k,c)
    if k==0
        return w*(1+c*r)
    else
        return exp.(-im*c*k).*(im*r*w*k.*cos.(w*k/2).+2*(-im*r.+k).*sin.(w*k/2))./k.^2
    end
end;

function u0hat(k,c,r)
    return u0hat(k,c)
end

# Functions for evaluation
################

# parameters of evaluation
jmax=10000; # we will sum over 2*jmax+1 terms
js=collect(-jmax:jmax);

# definition of u
include("defu.jl")

# Plotting parameters
################
# define the mesh in variable x
xstep = L/1000
X = collect(0:xstep:L)
# define the times at which to plot
(p,q) = (TimeNumerator,TimeDenominator);
T = p*L^2/(4*pi*q);
cs=round.(collect(w:w/3:L-w);digits=3)
