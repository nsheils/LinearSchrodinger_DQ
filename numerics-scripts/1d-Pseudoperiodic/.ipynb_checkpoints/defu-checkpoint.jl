# definition of u

#This definition comes from DLMF (https://dlmf.nist.gov/4.23#F1)
z=(1+beta0*beta1)/(beta0+beta1);
if imag(z)==0 && real(z)>=1
    gamma=(z+sqrt(z^2-1)) #be careful of sign
elseif imag(z)==0 && real(z)<=-1
    gamma=-1-(z-sqrt(z^2-1)) #be careful of sign
else
    gamma=z+im*sqrt(1-z^2)
end

k(j) = 1/L*(2*j*pi .+ acos((1+beta0*beta1)/(beta0+beta1)));
nor=(beta0*gamma-1)*(beta1*gamma-1)/((1+beta0*beta1)*(gamma^2+1)-2*gamma*(beta0+beta1));
bterm(j,x) = exp.(im*k(j)*x)+(gamma-beta0)/(beta0-1/gamma)*exp.(-im*k(j)*x);
pterm(j,t,c,r) = exp.(-im*k(j).^2*t).*(u0hat(k(j),c,r)+(beta1-gamma)/(gamma*(1-gamma*beta1))*u0hat(-k(j),c,r));
u(x,t,c,r) = nor/L*transpose(transpose(pterm(js,t',c,r))*bterm(js,x'))
