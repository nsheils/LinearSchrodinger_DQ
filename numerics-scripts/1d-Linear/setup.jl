# Load ARGS
################
try
    IntervalLength = parse(Float64,ARGS[1]);
    BCbeta11 = parse(Float64,ARGS[2]);
    BCbeta12 = parse(Float64,ARGS[3]);
    BCbeta13 = parse(Float64,ARGS[4]);
    BCbeta14 = parse(Float64,ARGS[5]);
    BCbeta22 = parse(Float64,ARGS[6]);
    BCbeta23 = parse(Float64,ARGS[7]);
    BCbeta24 = parse(Float64,ARGS[8]);
    IDwidth = parse(Float64,ARGS[9]);
    IDcenter= parse(Float64,ARGS[10]);
    IDslope = parse(Float64,ARGS[11]);
catch
    # print information on the error and quit gracefully.
    #need to update for this problem
    #print("If you want to manually specify the parameters, then call this script directly, passing through ARGS 7 arguments, the first 6 parsable as Float64, and the seventh parsable as Int64.\nWill try to use already defined arguments instead.\n")
end

# Parameters of problem
################
# Length of interval
L = IntervalLength;
# Coupling coeficients
(beta11,beta12, beta13, beta14, beta22, beta23, beta24) = (BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24);
# Check the problem is well-posed
# Not sure how to get this to throw an error on fail
cond1=beta11*beta22-beta14*beta23+beta13*beta24;
cond2=beta14*beta22-beta12*beta24;
cond3= beta13*beta22-beta12*beta23+beta11*beta24;
cond4=beta11*beta23

if cond1!=0 || cond2!=0 || cond3!=0 || cond4!=0
else
    println("ERROR: The boundary conditions chosen are inadmissable, this problem is ill-posed")
end
#must be zero for a zero-mode to exist.
#if a zero-mode exists then k=0 is a residue of at least order 3
if det([beta11+beta13+beta12*L beta12+beta14; beta22*L+beta23 beta22+beta24])==0
    print("ERROR: The boundary conditions chosen are inadmissable, 0 must not be an eigenvalue")
end

# Initial datum
################

# Parameters
(w,c,r) = (IDwidth,IDcenter,IDslope)
# Check the parameters make sense
# not implemented

# Initial datum
function u0(x,c,r)
    if x>c-w/2 && x<c+w/2
        return (x-c)*r+1
    else
        return 0
    end
end;

# Fourier transform of initial datum
function u0hat(k,c,r)
    if k==0
        return w*(1+c*r)
    else
        return exp.(-im*c*k).*(im*r*w*k.*cos.(w*k/2) .+ 2*(-im*r .+ k).*sin.(w*k/2))./k.^2
    end
end;

# Functions for evaluation
################
include("eval-fxns.jl")

# definition of kappa_j
include("def-eval.jl")

# definition of u
include("defu.jl")

# Plotting parameters
################
# define the mesh in variable x
xstep = L/500
X = collect(0:xstep:L)
# define the times at which to plot
tstep=.09;
tmin=0;
tmax=5.5;
ts=collect(tmin:tstep:tmax)

# create a p/q that approx matches the times selected above.
ps,qs = PSQS(ts);
