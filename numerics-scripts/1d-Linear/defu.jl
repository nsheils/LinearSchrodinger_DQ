# definition of u

#solution via separation of variables
b1a(k)=-(exp.(im*k*L).*(im*k*beta11.+beta12).+im*k*beta13.+beta14)./(exp.(-im*k*L).*(-im*k*beta11.+beta12).-im*k*beta13.+beta14);
b1b(k)=-(exp.(im*k*L)*beta22+im*k*beta23.+beta24)./(exp.(-im*k*L)*beta22-im*k*beta23.+beta24)
if sum(isnan.(b1a(k_j)))>0
    b1(k)=b1b(k);
elseif sum(isnan.(b1b(k_j)))>0
    b1(k)=b1a(k)
else
    k_j=k_j[exp.(-im*k_j*L)*beta22-im*k_j*beta23.+beta24.!=0]
    b1(k)=b1b(k)
end

b2bar1(k)=(-beta14*beta22.+beta12*beta24.-im*k*beta11.*(exp.(im*k*L)*beta22.+beta24))./(-im*exp.(im*L*k)*beta11*beta22.*k+exp.(2*im*L*k).*(beta14*beta22-beta24*beta12.-im*beta11*beta24*k))
b2bar2(k)=(exp.(im*L*k)*beta11*beta22.+beta13*beta22.-beta12*beta23+im*k*beta11*beta23)./(-exp.(im*L*k)*beta11*beta22+exp.(2*im*k*L).*(beta12*beta23-beta13*beta22.+im*k*beta11*beta23))

if sum(isnan.(b2bar1(k_j)))>0
    b2bar(k)=b2bar2(k);
else
    b2bar(k)=b2bar1(k)
end

xpart(x,k)=exp.(im*k*x')+b1(k).*exp.(-im*k*x');
cj(k)=1/L*(u0hat.(k,c,r)+b2bar(k).*u0hat.(-k,c,r))./(1 .+b1(k).*b2bar(k)+sin.(k*L)./(k*L).*(b1(k).*exp.(-im*k*L)+b2bar(k).*exp.(im*k*L)));

u(x,t,c,r)=transpose(xpart(x,k_j))*(exp.(-im*k_j.^2*t').*cj(k_j))
