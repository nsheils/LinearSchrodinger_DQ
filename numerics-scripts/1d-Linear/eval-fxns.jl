function sort_zeros(z::Array{Any,1})
    zchop!(z) #round to zero if within 10e-14
    filter!(zj->abs(zj)>1e-2, z::AbstractArray) #remove 0 since it is a removable singularity of my function.
    filter!(zj->real(zj)>-1e-5, z::AbstractArray) #keep only values in quadrant I and IV since I know Delta is odd.
    #if zero is purely imaginary then I only want to keep the one with positive imag part
    imag_ks=z[abs.(real(z)).<1e-5]
    if length(imag_ks)>0 && sum(imag(imag_ks).<0)>0
        imag_neg=imag_ks[imag(imag_ks).<0]
        z=z[z.!=imag_neg]
    end
    sort!(z,by=real)
    #throw out the roots that are too close together
    #note that I'm not using round so I can preserve accuracy.
    if length(diff(z))>0
        while minimum(abs.(diff(z)))<1e-3
            z_diff=abs.(diff(z))
            for j in 1:length(z_diff)
                if z_diff[j]<1e-3
                    z[j]=mean([z[j] z[j+1]])
                    z[j+1]=z[j]
                end
            end
            z=unique(z)
        end
        sort!(z,by=imag)
        while minimum(abs.(diff(z)))<1e-3
            z_diff=abs.(diff(z))
            for j in 1:length(z_diff)
                if z_diff[j]<1e-3
                    z[j]=mean([z[j] z[j+1]])
                    z[j+1]=z[j]
                end
            end
            z=unique(z)
        end
    end
    return sort!(z,by=real)
end

function find_Delta_imagroots(rad)
    #This is a specialized case of find_Delta_complexroots for purely imaginary roots
    #rad is radius of circle where I'm looking for roots.  Adjust according to graphs above.
    #Note...problems will occur if I'm looking for too many roots in too small of a circle. 
    k_j=[];
    
    z= Fun(t->rad*exp(im*pi*t),-1..1) #circle centered at origin with radius rad
    Df=Delta(z)
    s(y)=sum(z.^y.*Df'./Df)/(2*im*pi);
    ee1=s(1);
    ee2=1/2*(ee1.^2-s(2));
    ee3=1/3*(ee2*ee1-ee1*s(2)+s(3));
    ee4=1/4*(ee3*ee1-ee2*s(2)+ee1*s(3)-s(4))
    ee5=1/5*(ee4*ee1-ee3*s(2)+ee2*s(3)-ee1*s(4)+s(5))
    ee6=1/6*(ee5*ee1-ee4*s(2)+ee3*s(3)-ee2*s(4)+ee1*s(5)-s(6))
    p=[ee6,-ee5, ee4, -ee3, ee2, -ee1,1]; #Finding 6 roots each time.
    append!(k_j,PolynomialRoots.roots(p))
    return sort_zeros(k_j)
end

function find_Delta_complexroots(rad, minroot, maxroot)
    r=[];
    #rad is radius of circle where I'm looking for roots. Adjust according to graphs above. Mostly concerned with spacing.
    #Note...problems will occur if I'm looking for too many roots in too small of a circle. 
    #@showprogress 1 "Finding Roots..." 
    for n in collect(minroot:rad:maxroot)
        z= Fun(t->rad*exp(im*pi*t)+n,-1..1) #circle centered at (n,0) with radius rad
        Df=Delta(z)
        #oneoverDf=1/Delta(z) #this is a huge time suck but will help get accurate Fun s
        #s(k)=sum(z.^k.*Df'.*oneoverDf)/(2*im*pi);
        s(y)=sum(z.^y.*Df'./Df)/(2*im*pi);
        ee1=s(1);
        ee2=1/2*(ee1.^2-s(2));
        p=[ee2, -ee1,1]; #Finding 2 roots each time.
        append!(r,PolynomialRoots.roots(p))
    end
    return sort_zeros(r)
end

function get_more_realzeros(k_j,my_diff)
    ind_old=[];
    ind=findall(z->z>my_diff,abs.(diff(k_j)))
    while length(ind)>length(ind_old) #run until I'm not finding any more roots
        ind=findall(z->z>my_diff,abs.(diff(k_j)))
        #@showprogress 1 "Finding Roots..." 
        for j in 1:length(ind)
            r=real(k_j[ind[j]]);
            append!(k_j,Roots.find_zeros(absD,r,r+my_diff))
        end
        ind_old=ind
        k_j=sort_zeros(k_j);
    end
    return sort_zeros(k_j)
end

function find_small_roots()
    #Find all roots with "small" absolute value.  Includes finding complex and purely imaginary roots.
    k_j=find_Delta_imagroots(6) 
    step=50;
    minroot=0;
    maxroot=minroot+step;
    #change rad based on where the complex roots are in above
    rad=1;
    while maximum(abs.(Delta.(k_j)))<1e-3 && maxroot<7e4
        append!(k_j,find_Delta_complexroots(rad,minroot,maxroot))
        minroot=maxroot
        maxroot=minroot+step;
        #print("maxroot is: ", maxroot)
        #print("error is: ", maximum(abs.(Delta.(k_j))))
    end
    k_j=k_j[abs.(Delta.(k_j)).<1e-3] #make sure none of the k_j are bad
    return sort_zeros(k_j)
 end

function focused_complex_zeros(k_j)
    #This uses the complex root finder I wrote on a focused interval to find more roots.
    #Not super accurate...I add values that give Delta(la)<.01 but this still helps the final computation.
    L_k_j=0;
    iter=1;
    my_diff=ceil(mean(abs.(diff(k_j[5:15]))))+1
    rad=my_diff/2.
    while length(k_j)>L_k_j && L_k_j<2e3 && iter<10 #keep going as long as I'm getting new roots
        ind=findall(z->z>my_diff,abs.(diff(k_j)))
        minroot=real(k_j[ind[1]])
        #Only go halfway through ind to keep things at a reasonable time
        maxroot=minimum([a_roots[end] real(k_j[ind[Int(floor(length(ind)/2))]])])
        L_k_j=length(k_j)
        iter+=1;
        r=find_Delta_complexroots(rad,minroot,maxroot)
        r=r[minroot.<real.(r).<maxroot];
        append!(k_j,r[abs.(Delta.(r)).<.01])
        k_j=sort_zeros(k_j[abs.(Delta.(k_j)).<.01])
        #println("The number of eigenvalues found (so far) is ", length(k_j))
        if iter==10
            print("This was stopped because it was taking too long.  Could be run again to find more roots")
        end
    end
    return k_j
end