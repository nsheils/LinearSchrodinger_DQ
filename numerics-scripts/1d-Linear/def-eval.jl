# definition of \kappa_j (eigenvalues)

Delta1=beta11*beta22-beta14*beta23+beta13*beta24;
Delta2=beta13*beta22-beta12*beta23+beta11*beta24;
Delta3(k)=-beta14*beta22+beta12*beta24.+beta11*beta23*k.^2;
Delta(k)=2*im*(k.*Delta1+k.*Delta2.*cos.(L*k)+Delta3(k).*sin.(L*k));

Deltap(k)=2*im*(Delta1+(Delta2+L*Delta3(k)).*cos.(L*k)+(-L*Delta2+2*beta11*beta23).*k.*sin.(L*k));
Deltapp(k)=2*im*L*(k.*cos.(L*k).*(4*beta11*beta23-L*Delta2)+(-Delta2*(1+L)-L*Delta3(k)+2*beta11*beta23).*sin.(L*k));



 k_j=find_small_roots()

#If the zeros are still complex then our beta_{ij} are inadmissable.
if abs(imag(k_j[end]))>1e-3
    print("ERROR: The boundary conditions chosen are inadmissable, the eigenvalues must approach real values")
end
println("The number of eigenvalues found (so far) is ", length(k_j))
#println("With maximum error of ", maximum(abs.(Delta(k_j))))

#Use this to find remaining real roots up to ~2*pi*NN
NN=1e4;

absD(kappa)=abs(Delta(kappa));
absDp(kappa)=abs(Deltap(kappa));
absDpp(kappa)=abs(Deltapp(kappa));
#the difference between k_j values should approach a constant value
my_diff=ceil(mean(abs.(diff(k_j[5:15]))))+1

#a_roots is the vector of roots that k_j should approach for large values.
firstbad=length(k_j);
a_roots=[];
if beta11*beta23==0
    a_roots=real(1/L*append!([2*pi*n+acos(-Delta1/Delta2) for n in 0:NN],[2*pi*n-acos(-Delta1/Delta2) for n in 0:NN]))
else
    a_roots=real(1/L*[pi*n for n in 0:2*NN])
end
append!(a_roots,-a_roots)
a_roots=sort!(a_roots[(a_roots.>real(k_j[firstbad]-my_diff))])

#try both methods in order of speed
#use try/ catch to avoid stopping when a root isn't found

#@time 
for la in a_roots
    try
        append!(k_j,Roots.halley(absD,absDp,absDpp,la))
    catch
        try
            append!(k_j,Roots.newton(absD,absDp,la))
        catch
        end
    end
end

k_j=sort_zeros(k_j); 
if len(k_j)>2e3
	println("The TOTAL number of eigenvalues found is ", length(k_j))
else 
	println("The number of eigenvalues found (so far) is ", length(k_j))
#println("With maximum error of ", maximum(abs.(Delta(k_j))))

ind=findall(z->z>my_diff,abs.(diff(k_j)))
if length(k_j)<2e3 && isempty(ind)==false #only run if I need more roots
    k_j=focused_complex_zeros(k_j)
    if length(k_j)>1e3
        println("The TOTAL number of eigenvalues found is ", length(k_j))
        #println("With maximum error of ", maximum(abs.(Delta(k_j))))
    else
        println("The number of eigenvalues found (so far) is ", length(k_j))
        #println("With maximum error of ", maximum(abs.(Delta(k_j))))
    end
    #plot(abs.(diff(k_j)),".-")
end

#We have assumed we have (nonzero) roots of order 1
if minimum(abs.(Deltap.(k_j)))<1e-2
    print("ERROR: The boundary conditions chosen are inadmissable, the eigenvalues must be of order 1")
end

##Define asymptotic roots for scatter plot
    a_roots_small=[];
    if beta11*beta23==0
        a_roots_small=real(1/L*append!([2*pi*n+acos(-Delta1/Delta2) for n in 0:15],[2*pi*n-acos(-Delta1/Delta2) for n in 0:15]));
    else
        a_roots_small=real(1/L*[pi*n for n in 0:2*15]);
    end
    append!(a_roots_small,-a_roots_small)
    filter!(zj->abs(zj)>1e-2, a_roots_small) #remove 0 since it is a removable singularity of my function.
    filter!(zj->real(zj)>-1e-5, a_roots_small) #keep only values in quadrant I and IV since I know Delta is odd.
    a_roots_small=sort!(unique(a_roots_small), by=real)
