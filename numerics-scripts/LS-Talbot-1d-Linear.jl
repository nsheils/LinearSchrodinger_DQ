# Prepares to plot u(t,x) for 1D problem with linear boundary conditions

# Indicate which experiments to run
################
ExpList = (true,true,true,true,true);

using PyPlot;
using LinearAlgebra;
using ZChop;
using ApproxFun;
using PolynomialRoots;
using Statistics;
using Roots;

# Functions
################

function PSQS(TS)
    myeps=.1/L;
    # print(TS)
    # print(length(TS))
    ps=Array{Int64}(undef,length(TS));
    qs=Array{Int64}(undef,length(TS));
    maxq=10;
    maxp=ceil((4*pi*tmax*maxq)/L^2);
    for j in 1:length(TS)
        t=TS[j]
        for q in 1:maxq, p in 0:maxp
            ps[j]=p
            qs[j]=q
            if 4*pi*t/L^2-myeps<p/q<4*pi*t/L^2+myeps
                break
            elseif j==1
                ps[j]=0
                qs[j]=1/myeps
            else
                ps[j]=ps[j-1]
                qs[j]=qs[j-1]
            end
        end
    end
    #the following should be not too big relative to length(ts).  If not, increase maxv.
    # if sum(1-(abs.(L^2*ps./(4*pi*qs)-TS).<myeps))>1
    #     print("ERROR: increase maxv")
    # end
    return ps,qs
end

# Run experiments
################
# Define master directory
outputsdir = string(@__DIR__,"/outputs");
ispath(outputsdir) ? println("Main output directory exists") : mkdir(outputsdir)
# Experiment 9
if ExpList[1] ##Generates frames necessary for Fig 10,11,12
    IntervalLength, BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24, IDwidth, IDcenter, IDslope = 1.0, -2.0, 1.0, 0, 0, 0, 0, 1, 0.25, 0.5, 0;
    ispath(string(outputsdir,"/Experiment9")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment9"))
    include("1d-Linear/setup.jl");
    ##################
    #  Scatter Plot of Eigenvalues #
    ##################
    fig = figure("eigenvalues",figsize=(5,3))
    scatter(real(a_roots_small[1:10]),imag(a_roots_small[1:10]),s=150,c="b",label="asymptotic values")
    scatter(real(k_j[1:10]),imag(k_j[1:10]),s=90,c="r",label=L"\kappa_j")
    title("First 10 eigenvalues")
    xlabel(L"\operatorname{Re}(\kappa_j)")
    ylabel(L"\operatorname{Im}(\kappa_j)")
    xlim(0,a_roots_small[10]+1)
    ylim(-abs(imag(k_j[1])+.1), abs(imag(k_j[1])+.1))
    legend(loc="upper right")
    tight_layout(w_pad=-1, h_pad=10, pad=0)
    savefig(string(outputsdir,"/Experiment9/evals.pdf"))
    clf()

    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        xlim(0,L)
        ylim(-1.05,1.05)
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment9/LS_irr_$j.pdf"));
        clf();
    end;
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        xlim(0,L)
        ylim(-1.05,1.05)
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment9/LS_r_$j.pdf"))
        clf()
    end
end

# Experiment 10
if ExpList[2] ##Generates frames necessary for Fig 13,14
    IntervalLength, BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24, IDwidth, IDcenter, IDslope = 1.0, -0.7, 1.0, 0, 0, 0, 0, 1.0, 0.25, 0.5, 0;
    ispath(string(outputsdir,"/Experiment10")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment10"))
    include("1d-Linear/setup.jl");
    ##################
    #  Scatter Plot of Eigenvalues #
    ##################
    fig = figure("eigenvalues",figsize=(5,3))
    scatter(real(a_roots_small[1:10]),imag(a_roots_small[1:10]),s=150,c="b",label="asymptotic values")
    scatter(real(k_j[1:10]),imag(k_j[1:10]),s=90,c="r",label=L"\kappa_j")
    title("First 10 eigenvalues")
    xlabel(L"\operatorname{Re}(\kappa_j)")
    ylabel(L"\operatorname{Im}(\kappa_j)")
    xlim(0,a_roots_small[10]+1)
    ylim(-abs(imag(k_j[1])+.1), abs(imag(k_j[1])+.1))
    legend(loc="upper right")
    tight_layout(w_pad=-1, h_pad=10, pad=0)
    savefig(string(outputsdir,"/Experiment10/evals.pdf"))
    clf()

    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        xlim(0,L)
        ylim(-1.05,1.05)
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment10/LS_irr_$j.pdf"));
        clf();
    end;
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        xlim(0,L)
        ylim(-1.05,1.05)
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment10/LS_r_$j.pdf"))
        clf()
    end
end

##Experiment 11
if ExpList[3] ##Generates frames necessary for Fig 15,16
    IntervalLength, BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24, IDwidth, IDcenter, IDslope = 1.0, 10.0, -13.0, 2.0, -0.1, 19.0, 1.0, 0.1, 0.25, 0.5, 0;
    ispath(string(outputsdir,"/Experiment11")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment11"))
    include("1d-Linear/setup.jl");
    ##################
    #  Scatter Plot of Eigenvalues #
    ##################
    fig = figure("eigenvalues",figsize=(5,3))
    scatter(real(a_roots_small[1:10]),imag(a_roots_small[1:10]),s=150,c="b",label="asymptotic values")
    scatter(real(k_j[1:10]),imag(k_j[1:10]),s=90,c="r",label=L"\kappa_j")
    title("First 10 eigenvalues")
    xlabel(L"\operatorname{Re}(\kappa_j)")
    ylabel(L"\operatorname{Im}(\kappa_j)")
    xlim(0,a_roots_small[10]+1)
    ylim(-abs(imag(k_j[1])+.1), abs(imag(k_j[1])+.1))
    legend(loc="upper right")
    tight_layout(w_pad=-1, h_pad=10, pad=0)
    savefig(string(outputsdir,"/Experiment11/evals.pdf"))
    clf()

    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        xlim(0,L)
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment11/LS_irr_$j.pdf"));
        clf();
    end;
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        xlim(0,L)
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment11/LS_r_$j.pdf"))
        clf()
    end
end

##Experiment 12
if ExpList[4] ##Generates frames necessary for Fig 17
    IntervalLength, BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24, IDwidth, IDcenter, IDslope = 1.0, 5.0, 0.5, 1.0, 0, .2, 0, 1.0, 0.25, 0.5, 0;
    ispath(string(outputsdir,"/Experiment12")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment12"))
    include("1d-Linear/setup.jl");
    ##################
    #  Scatter Plot of Eigenvalues #
    ##################
    fig = figure("eigenvalues",figsize=(5,3))
    scatter(real(a_roots_small[1:10]),imag(a_roots_small[1:10]),s=150,c="b",label="asymptotic values")
    scatter(real(k_j[1:10]),imag(k_j[1:10]),s=90,c="r",label=L"\kappa_j")
    title("First 10 eigenvalues")
    xlabel(L"\operatorname{Re}(\kappa_j)")
    ylabel(L"\operatorname{Im}(\kappa_j)")
    xlim(0,a_roots_small[10]+1)
    ylim(-abs(imag(k_j[1])+.1), abs(imag(k_j[1])+.1))
    legend(loc="upper right")
    tight_layout(w_pad=-1, h_pad=10, pad=0)
    savefig(string(outputsdir,"/Experiment12/evals.pdf"))
    clf()

    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        xlim(0,L)
        ylim(-1.05,1.05)
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment12/LS_irr_$j.pdf"));
        clf();
    end;
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        xlim(0,L)
        ylim(-1.05,1.05)
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment12/LS_r_$j.pdf"))
        clf()
    end
end

##Experiment 13
if ExpList[5] ##Generates frames necessary for Fig 18,19
    IntervalLength, BCbeta11, BCbeta12, BCbeta13, BCbeta14, BCbeta22, BCbeta23, BCbeta24, IDwidth, IDcenter, IDslope = 1.0, -4.0, im, 0, 0, 0, 0, 1.0, 0.25, 0.5, 0;
    ispath(string(outputsdir,"/Experiment13")) ? println("Experiment output directory exists") : mkdir(string(outputsdir,"/Experiment13"))
    include("1d-Linear/setup.jl");
    ##################
    #  Scatter Plot of Eigenvalues #
    ##################
    fig = figure("eigenvalues",figsize=(5,3))
    scatter(real(a_roots_small[1:10]),imag(a_roots_small[1:10]),s=150,c="b",label="asymptotic values")
    scatter(real(k_j[1:10]),imag(k_j[1:10]),s=90,c="r",label=L"\kappa_j")
    title("First 10 eigenvalues")
    xlabel(L"\operatorname{Re}(\kappa_j)")
    ylabel(L"\operatorname{Im}(\kappa_j)")
    xlim(0,a_roots_small[10]+1)
    ylim(-abs(imag(k_j[1])+.1), abs(imag(k_j[1])+.1))
    legend(loc="upper right")
    tight_layout(w_pad=-1, h_pad=10, pad=0)
    savefig(string(outputsdir,"/Experiment13/evals.pdf"))
    clf()

    for j=1:length(ts)
        fig=figure("LS_irrational_time",figsize=(5,3));
        xlim(0,L)
        ylim(-1.05,1.05)
        T = ts[j];
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=$T");
        title(mytitle);
        xlabel(L"x");
        legend(loc="upper right");
        tight_layout(w_pad=-1, h_pad=10, pad=0);
        savefig(string(outputsdir,"/Experiment13/LS_irr_$j.pdf"));
        clf();
    end;
    for j=1:length(ts)
        fig=figure("LS_rational_time",figsize=(5,3))
        xlim(0,L)
        ylim(-1.05,1.05)
        p=ps[j];
        q=qs[j];
        T = p*L^2/(4*pi*q);
        U = u(X,T,c,r)
        plot(X,real.(U),"b",label="\$\\operatorname{Re}(u(t,x))\$")
        plot(X,imag.(U),"b--",label="\$\\operatorname{Im}(u(t,x))\$")
        mytitle=latexstring("t=\\frac{$p}{$q} \\frac{L^2}{4\\pi}");
        title(mytitle)
        xlabel(L"x")
        legend(loc="upper right")
        tight_layout(w_pad=-1, h_pad=10, pad=0)
        savefig(string(outputsdir,"/Experiment13/LS_r_$j.pdf"))
        clf()
    end
end