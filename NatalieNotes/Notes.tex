% !TEX TS-program = pdflatexmk

\documentclass[11pt]{article}

\usepackage{fullpage}
\usepackage{latexsym}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath,amsthm}
\usepackage{subcaption}
\usepackage{color}
\usepackage{graphicx, graphics}
\usepackage{hyperref}
\usepackage{pstricks, pst-plot, epsfig}
\usepackage{xargs}
\usepackage{arydshln}
\setlength{\dashlinegap}{2pt}
\usepackage{verbatim}

\usepackage{lipsum} %this is used to create dummy text

\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Ann}{Ann}
\newcommand{\ud}{\,\mathrm{d}}
\renewcommand\Re{\operatorname{Re}}
\renewcommand\Im{\operatorname{Im}}


\def\Xint#1{\mathchoice
{\XXint\displaystyle\textstyle{#1}}%
{\XXint\textstyle\scriptstyle{#1}}%
{\XXint\scriptstyle\scriptscriptstyle{#1}}%
{\XXint\scriptscriptstyle\scriptscriptstyle{#1}}%
\!\int}
\def\XXint#1#2#3{{\setbox0=\hbox{$#1{#2#3}{\int}$}
\vcenter{\hbox{$#2#3$}}\kern-.5\wd0}}
\def\ddashint{\Xint=}
\def\dashint{\Xint-}

\definecolor{green}{RGB}{0,128,0}
\newcommand{\tr}{\textcolor{red}}
\newcommand{\tb}{\textcolor{blue}}

\newcommand{\CC}{\mathbb{C}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\ZZ}{\mathbb{Z}}

\newtheorem{lemma}{Lemma}
\newtheorem{prop}{Proposition}
\newtheorem{theo}{Theorem}
\newtheorem{conj}{Conjecture}
\newtheorem{defin}{Definition}
\newtheorem{ex}{Example}
\newtheorem{rem}{Remark}
\newtheorem{cor}{Corollary}
\newtheorem{ass}{Assertion}

\DeclareMathOperator{\erf}{erf}
\DeclareMathOperator{\sn}{sn}
\DeclareMathOperator{\cn}{cn}
\DeclareMathOperator{\dn}{dn}

%%%%
\graphicspath{{gfx/}}

\newcommand{\BE}{\begin{equation}}                                 %Begin an equation environment
\newcommand{\EE}{\end{equation}}                                   %End an equation environment
\newcommand{\BES}{\begin{equation*}}                               %Begin an equation* environment
\newcommand{\EES}{\end{equation*}}                                 %End an equation* environment
\newcommand{\D}{\ensuremath{\,\mathrm{d}}}						   %an upright d for infinitesimals			               
%%%

\begin{document}

\title{NOTES on UTM and DQ in Linear Schr\"odinger}
\date{\today}
%\author{Peter J. Olver$^1$ and Natalie E. Sheils$^2$ \\
%\footnotesize 1. School of Mathematics, University of Minnesota, Minneapolis, MN 55455\\ \footnotesize{\texttt{olver@umn.edu}}\\ \footnotesize{\url{http://www.math.umn.edu/~olver}}\\
%\footnotesize 2. School of Mathematics, University of Minnesota, Minneapolis, MN 55455\\ \footnotesize{\texttt{nesheils@umn.edu}}\\ \footnotesize{\url{http://www.math.umn.edu/~nesheils}}
%}

\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{abstract}

%\end{abstract}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linear Schr\"odinger}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Consider the linear Schr\"odinger equation:
\begin{subequations}\label{ls}
\begin{align}
iq_t+q_{xx}&=0, &&(x,t)\in[0,L]\times [0,T],\\
q(x,0)&=q_0(x),&&x\in[0,L].
\end{align}
\end{subequations}
We must prescribe two boundary conditions:
\begin{subequations}\label{ls_bcs}
\begin{align}
\beta_{11}q_x(L,t)+\beta_{12}q(L,t)+\beta_{13}q_x(0,t)+\beta_{14}q(0,t)&=f_1(t),\\
\beta_{22}q(L,t)+\beta_{23}q_x(0,t)+\beta_{24}q(0,t)&=f_2(t).
\end{align}
\end{subequations}

Now let's consider ``pseudo-periodic'' boundary conditions.
That is~\eqref{ls_bcs} with $\beta_{11}=-1=\beta_{22},\beta_{12}=\beta_{14}=\beta_{23}=f_1(t)=f_2(t)=0$:
\begin{align}
	q_x(L,t)=&\beta_{13}q_x(0,t)\\
	q(L,t)=&\beta_{24}q(0,t).
\end{align}

Energy is conserved if $\beta_{24}\beta_{13}=1$.
This is true in the periodic case.
In what follows we let $\beta_{24}=1/\beta_{13}$, for $\beta=\beta_{13}$ a nonzero real number.
The solution is
\begin{equation}
\begin{split}
	q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2t}\hat{q}_0(\lambda )\ud \lambda +\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2t}\left((1+\beta^2-2e^{i\lambda L}\beta)\hat{q}_0(\lambda )+(\beta^2-1)\hat{q}_0(-\lambda )\right)}{2\pi\Delta(\lambda )}\\
	&+\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2t}\left((2\beta-e^{i\lambda L}(1+\beta^2))\hat{q}_0(\lambda )+e^{-i\lambda L}(\beta^2-1)\hat{q}_0(-\lambda )\right)}{2\pi\Delta(\lambda )}
\end{split}
\end{equation}
where
\begin{equation}\label{energypreserv_pp_Delta}
	%\Delta(\lambda )=e^{-i\lambda L}(1+\beta^2)-4\beta+e^{i\lambda L}(1+\beta^2)
	\Delta(\lambda )=2\cos(\lambda L)(1+\beta^2)-4\beta
\end{equation}
The poles of the integrand occur only at the zeros of $\Delta(\lambda )$:
% \begin{align}
	% \lambda _\pm=&\frac{2\pi m}{L}-\frac{i}{L}\log\left(\frac{2\beta\pm i |\beta^2-1|}{1+\beta^2}\right)\\
	% =&\frac{2\pi m+\arg(2\beta\pm i|\beta^2-1|)}{L}
% \end{align}
$$
	\lambda _\pm = \frac{2\pi m}{L} \pm \frac{1}{L}\arccos\left( \frac{2\beta}{1+\beta^2} \right),
$$
for $m\in\ZZ$.
These zeros lie on the real-$\lambda $ axis and are spaced at intervals of $2\pi/L$.
%This is a bit misleading... $\lambda _-$ are spaced at an interval of $2\pi/L$ and so are the $\lambda _+$ but if you look at all the $\lambda _\pm$ together they are not necessarily spaced this way.
Every zero is simple, except in the
%quasi-periodic
case where $|\beta|=1$; then each zero is double.
The regions $\tilde{D}^\pm$ and their half-plane complements $\tilde{E}^\pm$ for this problem are given in Figure~\ref{fig:LS_Deps}.
\begin{figure}
   \centering
   \def\svgwidth{.5\textwidth}
   \input{gfx/LS_Deps.pdf_tex}
      \caption{The regions $\tilde{D}^\pm$ and $\tilde{E}^\pm$ for the linear Schr\"odinger equation.
   \label{fig:LS_Deps}}
\end{figure}

Following~\cite{KesiciPelloniPryerSmith, SmithThesis, Smith2012} let 
\begin{align}
	\zeta^+(\lambda ,t) &= (2\beta-e^{i\lambda L}(1+\beta^2))\hat{q}_0(\lambda )+e^{-i\lambda L}(\beta^2-1)\hat{q}_0(-\lambda ),\\
	\zeta^-(\lambda ,t) &= (1+\beta^2-2e^{i\lambda L}\beta)\hat{q}_0(\lambda )+(\beta^2-1)\hat{q}_0(-\lambda ).
\end{align}
Note that $\zeta^+(\lambda ,t)-e^{-i\lambda L}\zeta^-(\lambda ,t)=-\hat{q}_0(\lambda )\Delta(\lambda )$.  
Further $$\int_{\partial \tilde{D}^+}\cdot \ud \lambda =\int_{\RR}\cdot \ud \lambda -\int_{\partial \tilde{E}^+}\cdot \ud \lambda ,~~~~\int_{\partial \tilde{D}^-}\cdot \ud \lambda =-\int_{\RR}\cdot \ud \lambda -\int_{\partial \tilde{E}^-}\cdot \ud \lambda .$$
Using these conditions and combining the integrals we have a more symmetric form of the solution
\begin{equation} \label{eqn:q.Pseudo.Conserv}
	q(x,t)=\frac{-1}{2\pi}\left(\int_{\partial \tilde{E}^-}e^{i\lambda (x-L)-i\lambda ^2t} \frac{\zeta^-(\lambda ,t)}{\Delta(\lambda )}\ud \lambda +\int_{\partial \tilde{E}^+}e^{i\lambda x-i\lambda ^2t} \frac{\zeta^+(\lambda ,t)}{\Delta(\lambda )}\ud \lambda \right)
\end{equation}

\subsection{$|\beta|\neq 1$}
For the linear Schr\"{o}dinger equation with conservative pseudoperiodic boundary conditions, we have
\begin{align*}
	q(x,t) &= \frac{-1}{2\pi}\sum_{\substack{\mu\in\RR:\\\Delta(\mu)=0}} \int_{C(\mu,r)} e^{i\lambda x-i\lambda^2t} \frac{\zeta^+(\lambda)}{\Delta(\lambda)}\D\lambda \\ \notag
	&= i \sum_{\substack{\mu\in\RR:\\\Delta(\mu)=0}} \Res_{\lambda=\mu} \left[ e^{i\lambda x-i\lambda^2t} \frac{\zeta^+(\lambda)}{\Delta(\lambda)} \right] \\ \notag
	&= i \sum_{\substack{\mu\in\RR:\\\Delta(\mu)=0}} e^{i\mu x-i\mu^2t} \Res_{\lambda=\mu} \left[ \frac{\zeta^+(\lambda)}{\Delta(\lambda)} \right] \\
	&= i \sum_{\substack{\mu\in\RR:\\\Delta(\mu)=0}} e^{i\mu x-i\mu^2t} \frac{\zeta^+(\mu)}{\Delta'(\mu)},
\end{align*}
where the last equality holds if and only if all zeros of $\Delta$ are simple, which is equivalent to $|\beta|\neq1$.
The zeros of $\Delta$ are all real.
We define
\BE
	\lambda_j = \frac{1}{L}\left[2j\pi + \arccos\left( \frac{2\beta}{1+\beta^2} \right)\right],
\EE
so that $\{\lambda\in\CC:\Delta(\lambda)=0\} = \{\lambda_j,-\lambda_j:j\in\ZZ\}$.
Thus,
\BE
	\cos(L\lambda_j) = \frac{2\beta}{\beta^2+1}, \qquad\qquad \sin(L\lambda_j) = \frac{|\beta^2-1|}{\beta^2+1}.
\EE

For notational convenience, we define the modulus $1$ complex number
\BE
	\gamma = \frac{2\beta}{\beta^2+1} + i\frac{|\beta^2-1|}{\beta^2+1} = e^{i\lambda_jL},
\EE
which is independent of $j$.

We calculate
\BE
	\Delta'(\lambda_j) = -2L\left(1+\beta^2\right)\sin(L\lambda_j) = -2L|\beta^2-1| = -\Delta'(-\lambda_j).
\EE
%Because $\lambda_j$ is a zero of $\Delta$,
Using the definition of $\gamma$ we have,
\BE
	\zeta^+(\lambda_j) = |\beta^2-1|\left[\sgn(\beta^2-1)\gamma^{-1}\hat{q}_0(-\lambda_j)-i\hat{q}_0(\lambda_j)\right].
\EE
We use this to simplify the ratios
\begin{align*}
	\frac{\zeta^+(\lambda_j)}{\Delta'(\lambda_j)} &= \frac{1}{2L} \left[ i\hat{q}_0(\lambda_j) - \gamma^{-1}\sgn(\beta^2-1)\hat{q}_0(-\lambda_j) \right], \\
	\frac{\zeta^+(-\lambda_j)}{\Delta'(-\lambda_j)} &= \frac{1}{2L} \left[ i\hat{q}_0(-\lambda_j) + \gamma\sgn(\beta^2-1)\hat{q}_0(\lambda_j) \right] = -i\gamma\sgn(\beta^2-1)\frac{\zeta^+(\lambda_j)}{\Delta'(\lambda_j)}.
\end{align*}
We obtain the formula
\textcolor{red}{
\BE
	\label{eqn:Analytics.q.formula}
	q(x,t) = \frac{1}{2L} \sum_{j\in\ZZ} e^{-i\lambda_j^2t} \left[ -e^{i\lambda_j x} + i\sgn(\beta^2-1)\gamma e^{-i\lambda_j x} \right] \left(\hat{q}_0(\lambda_j) + \frac{i\sgn(\beta^2-1)}{\gamma}\hat{q}_0(-\lambda_j)\right),
\EE
which is valid for all $|\beta|\neq1$.
}

\textcolor{blue}{
Note that if we consider $q_0(x)=\delta(x-L/2)$ then $\hat{q}_0(\lambda_j)=\gamma^{-1/2}$ and $\hat{q}_0(-\lambda_j)=\gamma^{1/2}$.
Hence,~\eqref{eqn:Analytics.q.formula} reduces to
\BE
	\label{eqn:Analytics.q.DeltaIC}
	q(x,t) = \frac{1 + i\sgn(\beta^2-1)}{2L\gamma^{1/2}} \sum_{j\in\ZZ} e^{-i\lambda_j^2t} \left[ -e^{i\lambda_j x} + i\sgn(\beta^2-1)\gamma e^{-i\lambda_j x} \right],
\EE
}
We define $q_0^\sharp$ and $q_0^\flat$ to be scaled-periodic full line extensions of $q_0$ and $q_0(L-\cdot)$: 
%The idea is to reflect in $L/2$ and then make a scaled-periodic extension.

\begin{align*}
	q_0^\sharp(x;\gamma) &= \gamma^{m} q_0(x-mL), & &\mbox{where } m\in\ZZ \mbox{ is such that } mL \leq x < (m+1)L, \\
	q_0^\flat (x;\gamma) &= \gamma^{m} q_0(mL-x), & &\mbox{where } m\in\ZZ \mbox{ is such that } (m-1)L \leq x < mL.
\end{align*}
We define also the shifts of these extensions
\begin{align*}
	q_s^\sharp(x;\gamma) &= q_0^\sharp(x+s;\gamma), \\
	q_s^\flat (x;\gamma) &= q_0^\flat (x-s;\gamma).
\end{align*}
Then, for $s\in\RR$ and $n\in\ZZ$ such that $nL \leq s < (n+1)L$,
\begin{align*}
	\hat{q}_s^\sharp(\lambda ;\gamma) &= \int_0^L e^{-i\lambda x} q_s^\sharp(x;\gamma) \D x = \int_0^L e^{-i\lambda x} q_0^\sharp(x+s;\gamma) \D x \\
	&= \int_{0}^{(n+1)L-s} e^{-i\lambda x} \gamma^{n} q_0(x+s-nL) \D x + \int_{(n+1)L-s}^{L} e^{-i\lambda x} \gamma^{n+1} q_0(x+s-(n+1)L) \D x \\
	&= \gamma^{n} \int_{s-nL}^L e^{-i\lambda (y-s+nL)} q_0(y) \D y + \gamma^{n+1} \int_0^{s-nL} e^{-i\lambda (y-s+(n+1)L)} q_0(y) \D y.
\end{align*}
\begin{align*}
	\hat{q}_s^\flat(\lambda ;\gamma) &= \int_0^L e^{-i\lambda x} q_s^\flat(x;\gamma) \D x = \int_0^L e^{-i\lambda x} q_0^\flat(x-s;\gamma) \D x \\
	&= \int_{0}^{s-nL} e^{-i\lambda x} \gamma^{-n} q_0(-nL-x+s) \D x + \int_{s-nL}^{L} e^{-i\lambda x} \gamma^{-(n-1)} q_0(-(n-1)L-x+s) \D x \\
	&= \gamma^{-n} \int_{0}^{s-nL} e^{i\lambda (y+nL-s)} q_0(y) \D y +\gamma^{-(n-1)} \int_{s-nL}^{L} e^{i\lambda (y+(n-1)L-s)} q_0(y) \D y.
\end{align*}
Therefore, and by similar calculations, for $nL \leq s < (n+1)L$,
\begin{subequations}
\begin{align}
	\hat{q}_{ s}^\sharp(\lambda ;\gamma ) &= e^{i\lambda s} \left[
		\gamma^{n+1}e^{-iL(n+1)\lambda} \int_0^{s-nL} e^{-i\lambda y} q_0(y) \D y +
		\gamma^ne^{-i L n\lambda } \int_{s-nL}^L e^{-i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{ s}^\flat (\lambda ;\gamma ) &= e^{-i\lambda s} \left[
		\gamma^{-n}e^{i L n\lambda   } \int_0^{s-nL} e^{i\lambda y} q_0(y) \D y +
		\gamma^{-(n-1)}e^{i L(n-1) \lambda} \int_{s-nL}^L e^{i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\sharp(\lambda ;\gamma) &= e^{-i\lambda s} \left[
		\gamma^{-n}e^{i L n \lambda  } \int_0^{(n+1)L-s} e^{-i\lambda y} q_0(y) \D y +
		\gamma^{-(n+1)}e^{iL(n+1)\lambda} \int_{(n+1)L-s}^L e^{-i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\flat (\lambda ;\gamma) &= e^{ i\lambda s} \left[
		\gamma^{n+1}e^{-i L(n+1)\lambda} \int_0^{(n+1)L-s} e^{i\lambda y} q_0(y) \D y +
		\gamma^{n+2}e^{-i L(n+2) \lambda} \int_{(n+1)L-s}^L e^{i\lambda y} q_0(y) \D y \right],\\
	\hat{q}_{ s}^\sharp(-\lambda ;\gamma^{-1} ) &= e^{-i\lambda s} \left[
		\gamma^{-(n+1)}e^{iL(n+1)\lambda} \int_0^{s-nL} e^{i\lambda y} q_0(y) \D y +
		\gamma^{-n}e^{i L n\lambda } \int_{s-nL}^L e^{i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{ s}^\flat (-\lambda ;\gamma^{-1} ) &= e^{i\lambda s} \left[
		\gamma^n e^{-i L n \lambda   } \int_0^{s-nL} e^{-i\lambda y} q_0(y) \D y +
		\gamma^{n-1}e^{-i L(n-1)\lambda} \int_{s-nL}^L e^{-i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\sharp(-\lambda ;\gamma^{-1}) &= e^{i\lambda s} \left[
		\gamma^ne^{-i L n\lambda   } \int_0^{(n+1)L-s} e^{i\lambda y} q_0(y) \D y +
		\gamma^{n+1}e^{-iL(n+1)\lambda)} \int_{(n+1)L-s}^L e^{i\lambda y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\flat (-\lambda ;\gamma^{-1}) &= e^{- i\lambda s} \left[
		\gamma^{-(n+1)}e^{i L(n+1)\lambda} \int_0^{(n+1)L-s} e^{-i\lambda y} q_0(y) \D y +
		\gamma^{-(n+2)}e^{i L(n+2)\lambda} \int_{(n+1)L-s}^L e^{-i\lambda y} q_0(y) \D y \right].
	\end{align}
\end{subequations}

Evaluating some of the above at $\lambda=\pm\lambda_j$ we have
\begin{subequations}\label{eqn:shap_flat_q0}
\begin{align}
	\hat{q}_{ s}^\sharp(\lambda_j;\gamma ) =& e^{i\lambda_j s} \hat{q}_0(\lambda_j), \\
%	\hat{q}_{ s}^\flat (\lambda_j;\gamma ) = &e^{-i\lambda_j s} \hat{q}_0(-\lambda_j), \\
%	\hat{q}_{-s}^\sharp(\lambda_j;\gamma) = &e^{-i\lambda_j s} \hat{q}_0(\lambda_j) , \\
	 \hat{q}_{-s}^\flat (\lambda_j;\gamma) = &e^{ i\lambda_j s} \hat{q}_0(-\lambda_j),\\
%	\hat{q}_{ s}^\sharp(-\lambda_j;\gamma^{-1} ) =& e^{-i\lambda_j s} \hat{q}_0(-\lambda_j), \\
	 \hat{q}_{ s}^\flat (-\lambda_j;\gamma^{-1} ) = &e^{i\lambda_j s} \hat{q}_0(\lambda_j), \\
	\hat{q}_{-s}^\sharp(-\lambda_j;\gamma^{-1}) =& e^{i\lambda_j s} \hat{q}_0(-\lambda_j) , \\
%	\hat{q}_{-s}^\flat (-\lambda_j;\gamma^{-1}) = &e^{ -i\lambda_j s} \hat{q}_0(\lambda_j),\\
	\hat{q}_{ s}^\sharp(-\lambda_j;\gamma ) =& e^{-i\lambda_j s} \left[
		\gamma^{2(n+1)} \int_0^{s-nL} e^{i\lambda_j y} q_0(y) \D y +
		\gamma^{2n} \int_{s-nL}^L e^{i\lambda_j y} q_0(y) \D y \right], \\
%	\hat{q}_{ s}^\flat (-\lambda_j;\gamma ) =& e^{i\lambda_j s} \left[
%		\gamma^{-2n} \int_0^{s-nL} e^{-i\lambda_j y} q_0(y) \D y +
%		\gamma^{-2(n-1)}\int_{s-nL}^L e^{-i\lambda_j y} q_0(y) \D y \right], \\
%	\hat{q}_{-s}^\sharp(-\lambda_j;\gamma) =& e^{i\lambda_j s} \left[
%		\gamma^{-2n} \int_0^{(n+1)L-s} e^{i\lambda_j y} q_0(y) \D y +
%		\gamma^{-2(n+1)} \int_{(n+1)L-s}^L e^{i\lambda_j y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\flat (-\lambda_j;\gamma) &= e^{-i\lambda_j s} \left[
		\gamma^{2(n+1)} \int_0^{(n+1)L-s} e^{-i\lambda_j y} q_0(y) \D y +
		\gamma^{2(n+2)} \int_{(n+1)L-s}^L e^{-i\lambda_j y} q_0(y) \D y \right],\\
%	\hat{q}_{ s}^\sharp(\lambda_j;\gamma^{-1} ) =& e^{i\lambda_j s} \left[
%		\gamma^{-2(n+1)} \int_0^{s-nL} e^{-i\lambda_j y} q_0(y) \D y +
%		\gamma^{-2n} \int_{s-nL}^L e^{-i\lambda_j y} q_0(y) \D y \right], \\
	\hat{q}_{ s}^\flat (\lambda_j;\gamma^{-1} ) =& e^{-i\lambda_j s} \left[
		\gamma^{2n}  \int_0^{s-nL} e^{i\lambda_j y} q_0(y) \D y +
		\gamma^{2(n-1)} \int_{s-nL}^L e^{i\lambda_j y} q_0(y) \D y \right], \\
	\hat{q}_{-s}^\sharp(\lambda_j;\gamma^{-1}) =& e^{-i\lambda_j s} \left[
		\gamma^{2n}  \int_0^{(n+1)L-s} e^{-i\lambda_j y} q_0(y) \D y +
		\gamma^{2(n+1)} \int_{(n+1)L-s}^L e^{-i\lambda_j y} q_0(y) \D y \right].
%	\hat{q}_{-s}^\flat (\lambda_j;\gamma^{-1}) =& e^{i\lambda_j s} \left[
%		\gamma^{-2(n+1)}  \int_0^{(n+1)L-s} e^{i\lambda_j y} q_0(y) \D y +
%		\gamma^{-2(n+2)} \int_{(n+1)L-s}^L e^{i\lambda_j y} q_0(y) \D y \right].
	\end{align}
\end{subequations}

Evaluating equation~\eqref{eqn:Analytics.q.formula} at $t=0$, we find
$$
q(x,0) =q_0(x)= \frac{1}{2L} \sum_{j\in\ZZ} \left[ -e^{i\lambda_j x} + i\sgn(\beta^2-1)\gamma e^{-i\lambda_j x} \right] \left(\hat{q}_0(\lambda_j)+\frac{i\sgn(\beta^2-1)}{\gamma} \hat{q}_0(-\lambda_j)\right).
$$
This is true not only for the initial datum $q_0(x)$, but for any reasonably well-behaved function defined on $[0,L]$.  
Hence, it is true for some $F(x;s)$:
\begin{equation}\label{eqn:Fq0}
F(x;s) = \frac{1}{2L} \sum_{j\in\ZZ} \left[ -e^{i\lambda_j x} + i\sgn(\beta^2-1)\gamma e^{-i\lambda_j x} \right] \left(\hat{F}(\lambda_j;s)+\frac{i\sgn(\beta^2-1)}{\gamma} \hat{F}(-\lambda_j;s)\right).
\end{equation}
In particular, choose
\BE \label{eqn:Asymptotics.defn.F}
	F(x;s)
	= q_s^\sharp(x;\gamma)
	+ \frac{i\sgn(\beta^2-1)}{2\gamma} q_{-s}^\flat(x;\gamma)
	-i\gamma\sgn(\beta^2-1)      q_{s}^\flat(x;\gamma^{-1})
	+ \frac{1}{2} q_{-s}^\sharp(x;\gamma^{-1}).
\EE

Using~\eqref{eqn:shap_flat_q0} we see that the parenthetical term on the right-hand-side of~\eqref{eqn:Fq0} can be written in terms of $\hat{q}_0(\cdot)$:
\begin{equation}
\hat{F}(\lambda_j;s)+\frac{i\sgn(\beta^2-1)}{\gamma} \hat{F}(-\lambda_j;s)=e^{i\lambda_js}\left(\hat{q}_0(\lambda_j)+\frac{i\sgn(\beta^2-1)}{\gamma}\hat{q}_0(-\lambda_j)\right).
\end{equation}
%\begin{subequations} \label{eqn:Analytics.qsharpflatLinComb}
%\begin{align}
%	\label{eqn:Analytics.qsharpflatLinComb1}
%	A \hat{q}_{ s}^\sharp(\lambda_j ) + i\gamma B \hat{q}_{ s}^\flat (-\lambda_j )
%	&=
%	(A+i\gamma B)e^{i\lambda_j s}\hat{q}_0( \lambda_j), \\
%	\label{eqn:Analytics.qsharpflatLinComb2}
%	Ai\gamma^{-1} \hat{q}_{ s}^\sharp(-\lambda_j ) + B \hat{q}_{ s}^\flat (\lambda_j )
%	&=%\gamma^{2n}(A+i\gamma^{-1} B)e^{i\lambda_j s}\hat{q}_0(- \lambda_j)
%	0, \\
%	\label{eqn:Analytics.qsharpflatLinComb3}
%	C \hat{q}_{-s}^\flat (\lambda_j) + i\gamma D \hat{q}_{-s}^\sharp(-\lambda_j )
%	&=
%	(C+i\gamma^{-1} D)e^{i\lambda_j s}\hat{q}_0(-\lambda_j), \\
%	\label{eqn:Analytics.qsharpflatLinComb4}
%	Ci\gamma^{-1} \hat{q}_{-s}^\flat (-\lambda_j) + D \hat{q}_{-s}^\sharp(\lambda_j )
%	&=%\gamma^{2n}(Ci\gamma^{-1}+D)e^{-i\lambda_j s}\hat{q}_0(- \lambda_j)
%	0,
%\end{align}
%\end{subequations}
%where equation~\eqref{eqn:Analytics.qsharpflatLinComb2} holds if $Ai\gamma^{-1}+B=0$, and equation~\eqref{eqn:Analytics.qsharpflatLinComb4} holds provided $Ci\gamma^{-1}+D=0$.  
%Choose $A=1/2$ and $C=i\sgn(\beta^2-1)/(2\gamma).$  

Finally, equation~\eqref{eqn:Fq0} becomes,
\BE \label{eqn:Analytic.F.looks.like.q}
F(x;s) = \frac{1}{2L} \sum_{j\in\ZZ}e^{i\lambda_js} \left[ -e^{i\lambda_j x} +i \sgn(\beta^2-1)\gamma e^{-i\lambda_j x} \right] \left(\hat{q}_0(\lambda_j)+\frac{i\sgn(\beta^2-1)}{\gamma}\hat{q}_0(-\lambda_j)\right).
\EE

Our goal is to show that whenever $t=\frac{p L^2}{q\pi}$ where $p,q\in\ZZ$ are co-prime, that $$q\left(x,\frac{p L^2}{q\pi}\right)=\sum_{m=0}^N e^{i c_m}F(x;s_m)$$ for some  $s_m\in\RR$ and $c_m\in\CC$.  
Recall, $$\lambda_j = \frac{1}{L}\left[2j\pi + \arccos\left( \frac{2\beta}{1+\beta^2} \right)\right].$$  
For notational convenience let $a=\arccos\left( \frac{2\beta}{1+\beta^2} \right).$  
Our goal is equivalent to finding $c_m\in\CC, s_m\in\RR$ such that
\begin{align}
\sum_{m=0}^N \exp(i c_m+i\lambda_j s_m)=&\exp\left(\frac{-i\lambda_j^2pL^2}{q\pi}\right)\\
=&\exp\left(\frac{-i(2j\pi+a)^2p}{q\pi}\right)
\end{align}
 for all $j\in\ZZ$. 
Then, we are trying to find $c_m\in\CC, s_m\in\RR$ such that
$$\sum_{m=0}^N c_m+\frac{s_m}{L}(2\pi j+a)+2\pi n_m=\frac{-4\pi p j^2}{q}-\frac{4pa }{q}-\frac{a^2 p}{q\pi}$$
%
Since this must be true for all $j$ we look at each order of $j$ individually. 
We note that $n_m$ must be of order $j^2$ since $-4\pi p j^2/q\neq 0.$  Hence, $$\sum_{m=0}^N n_m=-2j^2p/q.$$ 
\textcolor{blue}{THIS IS NOT POSSIBLE UNLESS $q=1,2$ SINCE EACH OF THE $n_m$ ARE INTEGERS.
%
Solving at orders $j^1$ and $j^0$ we find $$\sum_{m=0}^N s_m=\frac{-2pLa}{q\pi}\in\RR$$ and $$\sum_{m=0}^N c_m=\frac{a^2 p}{q\pi}\in\RR.$$  
These formulas make me think that this generalization is wrong because it doesn't help us at all.  
We may as well pick $N=0$.
}

\textcolor{blue}{
We have not used anything about the form of the initial conditions up to this point.
}
\medskip

\textcolor{blue}{\textbf{The following are notes from Dave:}
%
I think that the following is true, but I am still unsure how to use it.
%
\begin{conj} \label{conj:t-s.map.complicated}
	Suppose that, for some $q\in\NN$ and $p\in\ZZ$ coprime to $q$, it happens that $t=pL^2/(q\pi)$.
	Then there exists a real sequence $(s_m)_{m=0}^{q-1}$ and sequence of nonzero complex numbers $(c_m)_{m=0}^{q-1}$ such that, for all $j\in\ZZ$,
	\BE \label{eqn:t-s.map.complicated:exp.equality}
		j \equiv m \mod{q} \qquad\Rightarrow\qquad \exp \left( -i\lambda_j^2t \right) = \exp(ic_m) \exp \left(i\lambda_j s_m\right),
	\EE
	with $s_m,c_m$ independent of $j$.
\end{conj}
%
Of course, for $q=1$, this is simply the argument above.
%
I think this argument should generalise to give the conjecture.
%
But (even assuming it can be proved) is conjecture~\ref{conj:t-s.map.complicated} enough to get the result we want?
It seems like we would then be writing $q(x,t)$ not as a linear combination of $q$ different $F$ but as $b$ different \emph{projections of $F$, one projection onto each of the spaces spanned by the eigenfunctions of the spatial differential operator corresponding to eigenvalues indexed by $j\equiv m \mod q$}. 
Maybe that actually is the result, but it seems a little strange that we would then be getting (numerically) something that looks so much like shifts of copies of the initial datum, rather than shifts of copies of \emph{projections} of the initial datum.
%
I must be missing something obvious, but I still don't see it.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\bibliographystyle{plain}

%can support multiple bibliographies in the .bib style
\bibliography{./FullBib,./dbrefs}

\end{document}