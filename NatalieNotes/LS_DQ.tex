% !TEX TS-program = pdflatexmk

\documentclass[11pt]{article}

\input{texHead}

\begin{document}

\title{UTM and DQ in Linear Schr\"odinger}
\date{\today}

\maketitle


\section{Linear Schr\"odinger}
Consider the linear Schr\"odinger equation:
\begin{subequations}\label{ls}
\begin{align}
iq_t+q_{xx}&=0, &&(x,t)\in[0,L]\times [0,T],\\
q(x,0)&=q_0(x),&&x\in[0,L].
\end{align}
\end{subequations}

The local relation is \begin{equation}\label{ls_lr}
\left(e^{-i\lambda x+\omega t}q\right)_t-\left(e^{-i\lambda x+\omega t}(iq_x-\lambda q)\right)_x=0,
\end{equation}
with $\omega(\lambda )=i\lambda ^2$.
These are one parameter relations obtained by rewriting~\eqref{ls}.
Applying Green's Theorem in the strip $[0,L]\times [0,T]$, we find the global relations
\begin{equation}\label{ls_gr}
0=\hat{q_0}(\lambda )-e^{\omega T}\hat{q}(\lambda ,T)+e^{-i\lambda L}\left(h_1(\omega,T)-\lambda  h_0(\omega,T)\right)-\left(g_1(\omega,T)-\lambda  g_0(\omega,T)\right),
\end{equation}
where
\begin{subequations}\label{transforms}
\begin{align}
g_j(\omega,t)&=\int_0^t e^{\omega s}\partial_x^j q(x,s)\Big|_{x=0} \ud s,&t\in[0,T],&~j=0,1,\\
h_j(\omega,t)&=\int_0^t e^{\omega s}\partial_x^j q(x,s)\Big|_{x=L} \ud s,&t\in[0,T],&~j=0,1,\\
\hat{q}(\lambda ,t)&=\int_0^L e^{-i\lambda x}q(x,t)\ud x,&t\in(0,T], &~\lambda \in\CC,\\
\hat{q_0}(\lambda )&=\int_0^L e^{-i\lambda x}q_0(x)\ud x,&&~\lambda \in\CC.
\end{align}
\end{subequations}

Rearranging, evaluating at $T=t$, and inverting the Fourier transform in~\eqref{ls_gr} we find
\begin{equation}\label{ls_badsoln}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )\ud \lambda +\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda (x-L)-i\lambda ^2 t}\left(h_1(i\lambda ^2,t)-\lambda  h_0(i\lambda ^2,t)\right)\ud \lambda \\
&-\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\left(g_1(i\lambda ^2,t)-\lambda  g_0(i\lambda ^2,t)\right)\ud \lambda .
\end{split}
\end{equation}
This is not an effective solution formula because it involves all the boundary conditions.  
The integrands in~\eqref{ls_gr} are analytic in the entire complex-$\lambda $ plane.  With some foresight, we deform into the complex plane:
\begin{equation}\label{ls_badsoln_d}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )\ud \lambda -\frac{1}{2\pi}\int_{\partial D^-} e^{i\lambda (x-L)-i\lambda ^2 t}\left(h_1(i\lambda ^2,t)-\lambda  h_0(i\lambda ^2,t)\right)\ud \lambda \\
&-\frac{1}{2\pi}\int_{\partial D^+} e^{i\lambda x-i\lambda ^2 t}\left(g_1(i\lambda ^2,t)-\lambda  g_0(i\lambda ^2,t)\right)\ud \lambda ,
\end{split}
\end{equation}
where $D=D^+\cup D^-=\{\lambda \in\CC^\pm:\Re(i\lambda ^2)\leq0\}$ and the orientation of the integration path is such that the interior of the domain remains on the left as in Figure~\ref{fig:LS_D}.
\begin{figure}
   \centering
   \def\svgwidth{.5\textwidth}
   \input{gfx/LS_Dpm.pdf_tex}
      \caption{The regions $D^\pm$ for the linear Schr\"odinger equation.
   \label{fig:LS_D}}
\end{figure}

In what follows we will be concerned with poles of a function $\Delta(\lambda )$ (depending on the given boundary conditions).
To avoid any problems, we will deform further to
$$\tilde{D}^\pm=D^\pm\backslash \bigcup_{\substack{\lambda \in\CC\backslash\{0\}\\ \Delta(\lambda )=0}} \text{a small neighborhood of $\lambda $,}$$ is as in Figure~\ref{fig:LS_Deps}.
Since zero will be a removable singularity of all the integrands, we make an arbitrary finite contour deformation near zero.
\begin{figure}
   \centering
   \def\svgwidth{.5\textwidth}
   \input{gfx/LS_Deps.pdf_tex}
      \caption{The regions $\tilde{D}^\pm$ and $\tilde{E}^\pm$ for the linear Schr\"odinger equation.
   \label{fig:LS_Deps}}
\end{figure}
Thus,~\eqref{ls_badsoln_d} becomes
\begin{equation}\label{ls_badsoln_deps}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )\ud \lambda -\frac{1}{2\pi}\int_{\partial \tilde{D}^-} e^{i\lambda (x-L)-i\lambda ^2 t}\left(h_1(i\lambda ^2,t)-\lambda  h_0(i\lambda ^2,t)\right)\ud \lambda \\
&-\frac{1}{2\pi}\int_{\partial \tilde{D}^+} e^{i\lambda x-i\lambda ^2 t}\left(g_1(i\lambda ^2,t)-\lambda  g_0(i\lambda ^2,t)\right)\ud \lambda .
\end{split}
\end{equation}


\subsection{Periodic boundary conditions}
For a more complete discussion of the UTM for periodic problems see~\cite{TrogdonDecon}.
Assume periodic boundary conditions
\begin{subequations}\label{ls_bcs_periodic}
\begin{align}
q_x(L,t)&=q_x(0,t),\\
q(L,t)&=q(0,t).
\end{align}
\end{subequations}
Equivalently, $h_j(\omega,t)=g_j(\omega_t)$, $j=0,1$.  Then, the global relation becomes
\begin{equation}\label{ls_gr}
0=\hat{q_0}(\lambda )-e^{\omega t}\hat{q}(\lambda ,t)+\left(e^{-i\lambda L}-1\right)\left(g_1(\omega,t)-\lambda  g_0(\omega,t)\right).
\end{equation}
The unknown functions $g_j(\omega,t)$, $j=0,1$ are invariant under the transformation $\lambda \to-\lambda $.  Thus, we get a second global relation:
\begin{equation}\label{ls_gr_neg}
0=\hat{q_0}(-\lambda )-e^{\omega t}\hat{q}(-\lambda ,t)+\left(e^{i\lambda L}-1\right)\left(g_1(\omega,t)+\lambda  g_0(\omega,t)\right).
\end{equation}
Both global relations are valid for all $\lambda \in\CC$ in contrast to the problem posed for whole- or half-line problems.
Note that we can replace $g_j(\omega,t)$ with $g_j(\omega,T)$ in~\eqref{ls_badsoln_deps} using Jordan's Lemma and noting that $\int_0^T\cdot \ud s=\int_0^t \cdot \ud s+\int_t^T \cdot \ud s.$  
Solving~\eqref{ls_gr} and~\eqref{ls_gr_neg} for the unknown boundary conditions and plugging this into~\eqref{ls_badsoln_deps} we find the solution
\begin{equation}\label{ls_psoln_bad}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )\ud \lambda -\frac{1}{2\pi}\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2 t}\hat{q_0}(\lambda )}{\Delta(\lambda )}\ud \lambda -\frac{1}{2\pi}\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )}{\Delta(\lambda )}\ud \lambda \\
&+\frac{1}{2\pi}\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)+i\lambda ^2(T-t)}\hat{q}(\lambda ,T)}{\Delta(\lambda )}\ud \lambda +\frac{1}{2\pi}\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )}{\Delta(\lambda )}\ud \lambda ,
\end{split}
\end{equation}
where 
$$\Delta(\lambda )=1-e^{-i\lambda L}.$$
The poles of the integrands occur at at $\lambda =2n\pi/L$ for $n\in\ZZ$.  
The last two integrals in~\eqref{ls_psoln_bad} have integrands which are analytic and decay as $|\lambda |\to\infty$ from within the domains over which they are integrated.  Thus, using Cauchy's theorem and Jordan's Lemma these integrals evaluate to zero.  The effective solution to the BVP with periodic boundary conditions is given in terms of initial conditions as
\begin{equation}\label{ls_psoln}
q(x,t)=\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )\ud \lambda -\frac{1}{2\pi}\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2 t}\hat{q_0}(\lambda )}{\Delta(\lambda )}\ud \lambda -\frac{1}{2\pi}\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2 t}\hat{q_0}(\lambda )}{\Delta(\lambda )}\ud \lambda ,
\end{equation}
One can deform to the real line and recover the series solution as in~\cite{Olver}.

\subsection{Non-periodic boundary conditions}
For generic boundary conditions the two global relations for this problem are 
\begin{subequations}\label{ls_grs}
\begin{equation}\label{ls_gr1}
0=\hat{q_0}(\lambda )-e^{i\lambda ^2 t}\hat{q}(\lambda ,t)+e^{-i\lambda L}(h_1(i\lambda ^2,t)-\lambda h_0(i\lambda ^2,t))-(g_1(i\lambda ^2,t)-\lambda g_0(i\lambda ^2,t)),
\end{equation}
\begin{equation}\label{ls_gr2}
0=\hat{q_0}(-\lambda )-e^{i\lambda ^2 t}\hat{q}(-\lambda ,t)+e^{i\lambda L}(h_1(i\lambda ^2,t)+\lambda h_0(i\lambda ^2,t))-(g_1(i\lambda ^2,t)+\lambda g_0(i\lambda ^2,t)),
\end{equation}
\end{subequations}
which, again, are valid for all $\lambda \in\CC$.  

We must prescribe two boundary conditions:
\begin{subequations}\label{ls_bcs}
\begin{align}
\beta_{11}q_x(L,t)+\beta_{12}q(L,t)+\beta_{13}q_x(0,t)+\beta_{14}q(0,t)&=f_1(t),\\
\beta_{22}q(L,t)+\beta_{23}q_x(0,t)+\beta_{24}q(0,t)&=f_2(t).
\end{align}
\end{subequations}
Any $\beta_{ij}$ are allowed as long as at least one of the following holds:
\begin{enumerate}
\item $\beta_{11}\beta_{22}-\beta_{14}\beta_{23}+\beta_{13}\beta_{24}\neq0,$\\
\item $\beta_{14}\beta_{22}-\beta_{12}\beta_{24}\neq0,$\\
\item $\beta_{13}\beta_{22}-\beta_{12}\beta_{23}+\beta_{11}\beta_{24}\neq0,$\\
\item $\beta_{11}\beta_{23}\neq0$.
\end{enumerate}

For now, we will consider only the case when $f_1(t)\equiv 0\equiv f_2(t).$

Let \begin{equation}\begin{split}
\Delta(\lambda )=&e^{-i\lambda L}\left(\beta_{12}\beta_{24}-\beta_{14}\beta_{22}+\lambda (\beta_{13}\beta_{22}-\beta_{12}\beta_{23}+\beta_{11}\beta_{24})-\lambda ^2\beta_{11}\beta_{23}\right)\\
&+e^{i\lambda L}\left(\beta_{14}\beta_{22}-\beta_{12}\beta_{24}+\lambda (\beta_{13}\beta_{22}-\beta_{12}\beta_{23}+\beta_{11}\beta_{24})+\lambda ^2\beta_{11}\beta_{23}\right)\\
&+\beta_{11}\beta_{22}-\beta_{14}\beta_{23}+\beta_{13}\beta_{24}.
\end{split}
\end{equation}
Examining~\eqref{ls_badsoln_d} with the solutions of $h_1,h_0,g_1$, and $g_0$ found by using the two global relations~\eqref{ls_grs} and the two boundary conditions~\eqref{ls_bcs} we see that the terms involving $\hat{q}(\cdot,T)$ evaluate to 0 and our solution is 

%\begin{equation}
%\begin{split}
%q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-\omega t}\hat{q_0}(\lambda )\ud \lambda \\
%&+\int_{\partial D^-} \frac{e^{i\lambda (x-L)-i\lambda ^2(t-T)}}{2\pi\Delta(\lambda )}\left(2\lambda e^{i\lambda L}(\beta_{13}\beta_{24}-\beta_{14}\beta_{23})+\beta_{12}\beta_{24}-\beta_{14}\beta_{22}+\lambda (\beta_{13}\beta_{22}-\beta_{12}\beta_{23}+\beta_{11}\beta_{24})-\lambda ^2\beta_{11}\beta_{23} \right)\ud \lambda \\
%&-\frac{1}{2\pi}\int_{\partial D^+} e^{i\lambda x-i\lambda ^2 t}\left(g_1(i\lambda ^2,t)-\lambda  g_0(i\lambda ^2,t)\right)\ud \lambda ,
%\end{split}
%\end{equation}
%
%
\textcolor{red}{I could write this out but its fairly long...for now let's look at specific cases}

\subsubsection{Dirichlet Boundary Conditions}
Consider the case of Dirichlet boundary conditions on both sides:
\begin{subequations}\label{ls_bcs_d}
\begin{align}
q(L,t)&=0,\\
q(0,t)&=0.
\end{align}
\end{subequations}

Then, our solution becomes 
\begin{equation}\label{ls_soln_d}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2  t}\hat{q_0}(\lambda )\ud \lambda +\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2 t}(\hat{q_0}(\lambda )-\hat{q_0}(-\lambda ))}{2\pi\Delta(\lambda )}\ud \lambda \\
&+\int_{\partial \tilde{D}^+} \frac{e^{i\lambda  x-i\lambda ^2 t}(e^{i\lambda L}\hat{q_0}(\lambda )-e^{-i\lambda L}\hat{q_0}(-\lambda ))}{2\pi\Delta(\lambda )}\ud \lambda ,
\end{split}
\end{equation}
 where $$\Delta(\lambda )=e^{-i\lambda L}-e^{i\lambda L},$$
 and $\tilde{D}^\pm$ are as in Figure~\ref{fig:LS_Deps}.

If we deform $\partial \tilde{D}^\pm$ back to the real line we recover the Fourier series solution found using an odd extension of the initial condition:
\begin{equation}\label{ls_fssoln_d}
\begin{split}
q(x,t)=&\sum_{n=1}^\infty \frac{2}{L}e^{\frac{-in^2\pi^2 t}{L^2}}\sin\left(\frac{n\pi x}{L}\right)\int_0^L q_0(y)\sin\left(\frac{n\pi y}{L}\right)\ud y.
\end{split}
\end{equation}
Using a step function initial condition and summing up a few hundred Fourier modes I see dispersive quantization.


\subsubsection{Neumann boundary conditions}
Next we consider the case of Neumann boundary conditions on both sides:
\begin{subequations}\label{ls_bcs_n}
\begin{align}
q_x(L,t)&=0,\\
q_x(0,t)&=0.
\end{align}
\end{subequations}

Then, our solution becomes 
\begin{equation}\label{ls_soln_n}
\begin{split}
q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2  t}\hat{q_0}(\lambda )\ud \lambda +\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2 t}(\hat{q_0}(\lambda )+\hat{q_0}(-\lambda ))}{2\pi\Delta(\lambda )}\ud \lambda \\
&+\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2 t}(e^{i\lambda L}\hat{q_0}(\lambda )+e^{-i\lambda L}\hat{q_0}(-\lambda ))}{2\pi\Delta(\lambda )}\ud \lambda ,
\end{split}
\end{equation}
where $$\Delta(\lambda )=e^{-i\lambda L}-e^{i\lambda L},$$
and $\tilde{D}^\pm$ are as in Figure~\ref{fig:LS_Deps}


As before, if we deform $\partial \tilde{D}^\pm$ back to the real line we recover the Fourier series solution found using an even extension of the initial condition:
\begin{equation}\label{ls_fssoln_n}
\begin{split}
q(x,t)=&\frac{1}{L}\int_0^L q_0(y)\ud y+\frac{2}{L}\sum_{n=1}^\infty e^{\frac{-in^2\pi^2 t}{L^2}}\cos\left(\frac{n\pi x}{L}\right)\int_0^L q_0(y)\cos\left(\frac{n\pi y}{L}\right)\ud y.
\end{split}
\end{equation}
Using a step function initial condition and summing up a few hundred Fourier modes I see dispersive quantization.


\subsubsection{Energy conserving boundary conditions} \label{ssec:EnergyConservingBC}

Consider 
\begin{equation}\label{energy_ls}
\begin{split}
	\frac{\ud }{\ud t} \|q\|^2_2:=&\frac{\ud }{\ud t} \int_0^L |q |^2 \ud x=\frac{\ud }{\ud t} \int_0^L q \bar{q} \ud x =i\int_0^L q\bar{q}_{xx}-\bar{q}q_{xx}\ud x=i\left(q\bar{q}_x-\bar{q}q_x\right)\bigg|_{x=0}^{x=L}\\
	=&i\left(q(L,t)\bar{q}_x(L,t)-q(0,t)\bar{q}_x(0,t)-\bar{q}(L,t)q_x(L,t)+\bar{q}(0,t)q_x(0,t)\right)
\end{split}
\end{equation}
Obviously Dirichlet and Neumann boundary conditions considered in the previous sections conserve energy in time.
Now let's consider ``pseudo-periodic'' boundary conditions.
That is~\eqref{ls_bcs} with $\beta_{11}=-1=\beta_{22},\beta_{12}=\beta_{14}=\beta_{23}=f_1(t)=f_2(t)=0$:
\begin{align}
	q_x(L,t)=&\beta_{13}q_x(0,t)\\
	q(L,t)=&\beta_{24}q(0,t).
\end{align}
Assuming $\beta_{ij}\in\RR$,~\eqref{energy_ls} simplifies to
\begin{equation}\label{energy_ls_pp}
	\frac{\ud }{\ud t} \|q\|^2_2=i(\beta_{24}\beta_{13}-1)\left(q(0,t)\bar{q}_x(0,t)-\bar{q}(0,t)q_x(0,t)\right).
\end{equation}
Energy is conserved if $\beta_{24}\beta_{13}=1$.
This is true in the periodic case.
In what follows we let $\beta_{24}=1/\beta_{13}$, for $\beta_{13}$ a nonzero real number.
The solution is
\begin{equation}
\begin{split}
	q(x,t)=&\frac{1}{2\pi}\int_{-\infty}^\infty e^{i\lambda x-i\lambda ^2t}\hat{q}_0(\lambda )\ud \lambda +\int_{\partial \tilde{D}^-} \frac{e^{i\lambda (x-L)-i\lambda ^2t}\left((1+\beta_{13}^2-2e^{i\lambda L}\beta_{13})\hat{q}_0(\lambda )+(\beta_{13}^2-1)\hat{q}_0(-\lambda )\right)}{2\pi\Delta(\lambda )}\\
	&+\int_{\partial \tilde{D}^+} \frac{e^{i\lambda x-i\lambda ^2t}\left((2\beta_{13}-e^{i\lambda L}(1+\beta_{13}^2))\hat{q}_0(\lambda )+e^{-i\lambda L}(\beta_{13}^2-1)\hat{q}_0(-\lambda )\right)}{2\pi\Delta(\lambda )}
\end{split}
\end{equation}
where
\begin{equation}\label{energypreserv_pp_Delta}
	%\Delta(\lambda )=e^{-i\lambda L}(1+\beta_{13}^2)-4\beta_{13}+e^{i\lambda L}(1+\beta_{13}^2)
	\Delta(\lambda )=2\cos(\lambda L)(1+\beta_{13}^2)-4\beta_{13}
\end{equation}
The poles of the integrand occur only at the zeros of $\Delta(\lambda )$:
% \begin{align}
	% \lambda _\pm=&\frac{2\pi m}{L}-\frac{i}{L}\log\left(\frac{2\beta_{13}\pm i |\beta_{13}^2-1|}{1+\beta_{13}^2}\right)\\
	% =&\frac{2\pi m+\arg(2\beta_{13}\pm i|\beta_{13}^2-1|)}{L}
% \end{align}
\begin{equation}\label{lambdas}
	\lambda _\pm = \frac{2\pi m}{L} \pm \frac{1}{L}\arccos\left( \frac{2\beta_{13}}{1+\beta_{13}^2} \right),
\end{equation}
for $m\in\ZZ$.
These zeros lie on the real-$\lambda $ axis and are spaced at intervals of $2\pi/L$.
%This is a bit misleading... $\lambda _-$ are spaced at an interval of $2\pi/L$ and so are the $\lambda _+$ but if you look at all the $\lambda _\pm$ together they are not necessarily spaced this way.
Every zero is simple, except in the
%quasi-periodic
case where $|\beta_{13}|=1$; then each zero is double.
The regions $\tilde{D}^\pm$ and their half-plane complements $\tilde{E}^\pm$ for this problem are given in Figure~\ref{fig:LS_Deps}.

Following~\cite{KesiciPelloniPryerSmith, SmithThesis, Smith2012} let 
\begin{align}
	\zeta^+(\lambda ,t) &= (2\beta_{13}-e^{i\lambda L}(1+\beta_{13}^2))\hat{q}_0(\lambda )+e^{-i\lambda L}(\beta_{13}^2-1)\hat{q}_0(-\lambda ),\\
	\zeta^-(\lambda ,t) &= (1+\beta_{13}^2-2e^{i\lambda L}\beta_{13})\hat{q}_0(\lambda )+(\beta_{13}^2-1)\hat{q}_0(-\lambda ).
\end{align}
Note that $\zeta^+(\lambda ,t)-e^{-i\lambda L}\zeta^-(\lambda ,t)=-\hat{q}_0(\lambda )\Delta(\lambda )$.  
Further $$\int_{\partial \tilde{D}^+}\cdot \ud \lambda =\int_{\RR}\cdot \ud \lambda -\int_{\partial \tilde{E}^+}\cdot \ud \lambda ,~~~~\int_{\partial \tilde{D}^-}\cdot \ud \lambda =-\int_{\RR}\cdot \ud \lambda -\int_{\partial \tilde{E}^-}\cdot \ud \lambda .$$
Using these conditions and combining the integrals we have a more symmetric form of the solution
\begin{equation} \label{eqn:q.Pseudo.Conserv}
	q(x,t)=\frac{-1}{2\pi}\left(\int_{\partial \tilde{E}^-}e^{i\lambda (x-L)-i\lambda ^2t} \frac{\zeta^-(\lambda ,t)}{\Delta(\lambda )}\ud \lambda +\int_{\partial \tilde{E}^+}e^{i\lambda x-i\lambda ^2t} \frac{\zeta^+(\lambda ,t)}{\Delta(\lambda )}\ud \lambda \right)
\end{equation}

\textbf{Example}\\
Given $\beta_{13}=1+\sqrt{2}$ we can find an analytic expression for $\lambda _\pm$.  
Thus, we can deform the integrals in~\eqref{eqn:q.Pseudo.Conserv} to circles around $\lambda =\lambda _\pm$ using Jordan's Lemma and Cauchy's theorem, effectively giving a series representation of the solution.
In this case, the solution becomes
%could probably make this cleaner...
\begin{equation}\label{qp_ana_soln}
\begin{split}
q(x,t)=&\sum_{\substack{z=\lambda _\pm\\z<0}} e^{i z (x-L)-iz^2 t}\frac{(e^{iLz}(-2-\sqrt{2})+e^{2iLz}(1+\sqrt{2}))\hat{q}_0(z)-e^{iLz}(1+\sqrt{2})\hat{q}_0(-z)}{L(2+\sqrt{2})(e^{2iLz}-1)}\\
&+\sum_{\substack{z=\lambda _\pm\\z>0}} e^{i z x-iz^2 t}\frac{(e^{iLz}(-1-\sqrt{2})+e^{2iLz}(2+\sqrt{2}))\hat{q}_0(z)-(1+\sqrt{2})\hat{q}_0(-z)}{L(2+\sqrt{2})(e^{2iLz}-1)}.
\end{split}
\end{equation}
Choosing $L=2\pi$ and 
\begin{equation}\label{stepIC}
q_0(x)=\left\{\begin{array}{llr} 0,&&x<\pi,\\ 1,&& x>\pi,\end{array}\right.
\end{equation}
one can sum the series solution~\eqref{qp_ana_soln} using, for example, \textsc{Mathematica}.  
Using just the first 600 $\lambda _\pm$ for each of the sums in~\eqref{qp_ana_soln} we can plot $\|q(x,t)\|$ as in Figure~\ref{fig:qp_ana_soln}.
\begin{figure}[htb]
\begin{center}
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln0.pdf}
%\caption{$t=0$}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln1.pdf}
%\caption{$t=.5$}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln2.pdf}
%\caption{$t=1.$}
\end{subfigure}\\
\bigskip
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln5.pdf}
%\caption{$t=\pi/10$}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln3.pdf}
%\caption{$t=\pi/6$}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.3\textwidth}
\centering
\includegraphics[height=1.4in]{qp_ana_soln4.pdf}
%\caption{$t=\pi/3$}
\end{subfigure}
\caption{Fractal and piecewise-constant solution profiles to the linear Schr\"odinger equation~\eqref{ls} with step-function initial condition on a pseudo-periodic domain ($q(2\pi,t)= (\sqrt{2}-1)q(0,t)$, $q_x(2\pi,t)=(\sqrt{2}+1)q_x(0,t)$).}\label{fig:qp_ana_soln}
\end{center}
\end{figure}
We clearly see that for $t$ which is a rational multiple of $\pi$ the solution quantizes and for other values of $t$ the solution appears fractal.
\textcolor{red}{can this be proved as in~\cite{Olver}?}

\section{Location of the jumps}\label{sec:jumps}
We can numerically find the location of the jumps. 
We consider the case of energy-conserving boundary conditions ($\beta_{11}=-1=\beta_{22}, \beta_{12}=\beta_{14}=\beta_{23}=0, \beta_{13}=\beta$, and $\beta_{24}=1/\beta$).  
If $\beta=1$ then we are the case of periodic boundary conditions and we know that the solution quantizes whenever $t=\frac{\pi p}{q}$ for $p/q\in\mathbb{Q}$ a rational number and jumps in the solution occur at some of $x=\frac{\pi j}{q}$ for $j=0,1,\cdots 2q-1$. 
In Figure~\ref{fig:LS_jumps} we plot the $x$-location of jumps multiplied by $q/p$.  
When $|\beta|=1$ the values of $j$ are integers.  
For $\beta=2.5$ the values of $j$ are nearly integers, however, it does not appear that $j$ is an integer unless $|\beta|=1$.

\begin{figure}
   \centering
   \includegraphics[width=.75\textwidth]{gfx/LS_jumps.pdf}
      \caption{
   \label{fig:LS_jumps}}
\end{figure}

We would like to have an analytical representation for the location of the jumps.
To this end we consider as a possibility the zeros of the eigenfunctions.
Although we know in the case where $|\beta|\neq1$ the eigenfunctions do not form a complete basis we begin by separating variables
$$q(x,t)=X(x)T(t).$$
Plugging this assumption in to~\eqref{ls} we find
\begin{align}
iT'-\lambda^2T&=0\\
X''+\lambda^2X&=0,\\
X'(L)&=\beta_{13}X'(0),\\
X(L)&=\frac{1}{\beta_{13}}X(0).
\end{align}
In the case of energy conserving boundary conditions with $|\beta|\neq1$, from~\eqref{lambdas} we have $$\lambda=\lambda_\pm=\frac{2\pi m}{L} \pm \frac{1}{L}\arccos\left( \frac{2\beta_{13}}{1+\beta_{13}^2} \right)$$ where $m\in\ZZ.$
Solving the $X$ equation we find
$$X(x)=c_1\cos(\lambda x)+c_2\sin(\lambda x).$$
Using the boundary conditions we discover $$c_1=\mp c_2\beta_{13}\sgn(\beta_{13}^2-1)$$ and $c_2$ is free.  Hence,
$$X(x)=\mp \beta\sgn(\beta^2-1)\cos\left(\frac{2\pi m x}{L} \pm \frac{x}{L}\arccos\left( \frac{2\beta_{13}}{1+\beta_{13}^2} \right)\right)+\sin\left(\frac{2\pi m x}{L} \pm \frac{x}{L}\arccos\left( \frac{2\beta_{13}}{1+\beta_{13}^2} \right)\right),$$
where $m\in\ZZ$ and $|\beta|\neq1$.  
Again we can find the zeros of this function numerically.  It does not appear that they agree with the locations of the jumps in the solution.


\section{Evaluating Integrals} \label{sec:EvalInt}

The numerical evaluation integrals of the form~\eqref{eqn:q.Pseudo.Conserv} presents two challenges:
\begin{enumerate}
	\item[(i)]{
		Each integrand is a meromorphic function, whose finite singularities lie only at zeros of the shared denominator $\Delta$.
		As $\Delta$ will always be an exponential polynomial, the asymptotic locus of these zeros may be determined via the methods of Langer~\cite{Lan1931a}, and it is assured that all zeros are of finite order, and the zeros have no finite accumulation points.
		However, determining the precise location of each zero in closed form is, in general, not possible.
	}
	\item[(ii)]{
		In the transformation $D^\pm\mapsto\tilde{D}^\pm$, the boundary was adjusted to include some arcs within $D$.
		Therefore, there are some arcs of the integration contour $\partial\tilde{E}^\pm$ on which $\exp(-i\lambda ^2t)$ blows up exponentially in $t$.
		Indeed, such a large dominant term in a numerical integration will introduce very large floating point errors, preventing effective evaluation of the integral.
		Therefore, it is necessary to choose the offending arcs in such a way that they do not intrude too far into the interior of $D$, so that $\Re(-i\lambda ^2)$ is a sufficiently small positive number.
		However, the presence of a pole is another constraint preventing arbitrary minimisation of the contour.
	}
\end{enumerate}

We continue our study of the energy-conserving pseudo-periodic boundary conditions.
By choosing certain values of $\beta_{13}$, it is possible to find an analytic expression for $\lambda _\pm$.
For example, in the case $\beta_{13}=\sqrt{2}\pm1$, we obtain
$$
	\lambda _\pm = \frac{\pi(8m\pm1)}{4L}.
$$
In such particular cases, difficulty~(i) is avoided, and we shall use this example as a prototype.
In general, using ApproxFun~[CITATION]
%I hope! It can certainly be done using ChebFun.
it is possible to find the zeros to floating point accuracy, even for problems in which $\Delta$ is a more complicated exponential polynomial.

Difficulty~(ii) is more serious.
% Moreover, if one wishes to study third order problems also, the exponential factor $\exp(-i\lambda ^2t)$ will be replaced by $\exp(-i\lambda ^3t)$, which grows still more rapidly.
In order to minimise the floating point error, a first approach is to find the saddle point of each integrand close to each pole, and choose $\tilde{D}^\pm$ so that the integration contour passes through each saddle point.
An analytic first order approximation to the saddle point was employed in~\cite{KesiciPelloniPryerSmith}.
Efficiency might be improved by finding each saddle point to greater accuracy numerically, using ApproxFun.
But such methods can only hope to postpone difficulty~(ii) to greater $t$.

Writing the integrand of the first integral of equation~\eqref{eqn:q.Pseudo.Conserv} as
\begin{equation}
	e^{-i\lambda ^2t} \left[e^{i\lambda (x-L)}\zeta^-(\lambda ,t)\right] \frac{1}{\Delta(\lambda )},
\end{equation}
we observe three factors.
The first factor is entire and very large for $\lambda $ lying on each arc of $D \cap \partial\tilde{E}^-$.
The bracketed factor is entire and, relative to the first factor, reasonably well-behaved for $\lambda \in \partial\tilde{E}^-$.
The third factor has real poles of order 1 or 2, but is otherwise.

First deform the contour of integration from $\partial\tilde{E}^\pm$ to the union of countably many small circles, each centered at (a sufficiently good approximation of) a zero of $\Delta$.
% Explain this fully, using Jordan's lemma!
% It can certainly be done for all well-posed second order problems.
% I am pretty sure it can be done for all well-posed third order problems in which the zeros of $\Delta$ lie precisely on $\partial D$, but I'm not sure if the proof is given explicitly anywhere.

Suppose $\lambda _0>0$ is a zero of $\Delta$, and $\rho_0$ is the distance from $\lambda _0$ to the closest other zero of $\Delta$.
Fix $0<r<R < \rho_0/4$.
Suppose we can find a function $z(\lambda ;t)$, analytic on the disc $B(\lambda _0,2R)$, for which
\begin{equation}
	\max_{\lambda \in\Ann(\lambda _0,r,R)}\left(\left|\frac{1}{\Delta(\lambda )}-\frac{1}{\Delta(z(\lambda ;t))}\right| \times \left|e^{-i\lambda ^2t}\right|\right) < 1
\end{equation}
and also
\begin{equation}
	z(\lambda _0;t) \notin B(\lambda _0,3R).
\end{equation}
Then, on any contour $\gamma$ lying within the annulus $\Ann(\lambda _0,r,R)$,
$$
	\int_\gamma e^{-i\lambda ^2t} \left[e^{i\lambda (x-L)}\zeta^-(\lambda ,t)\right] \frac{1}{\Delta(z(\lambda ;t))} \ud \lambda  = 0,
$$
so
\begin{equation} \label{eqn:EasierContourIntegral}
	\int_\gamma e^{-i\lambda ^2t} \left[e^{i\lambda (x-L)}\zeta^-(\lambda ,t)\right] \left(\frac{1}{\Delta(\lambda )} - \frac{1}{\Delta(z(\lambda ;t))}\right) \ud \lambda  = \int_\gamma e^{-i\lambda ^2t} \left[e^{i\lambda (x-L)}\zeta^-(\lambda ,t)\right] \frac{1}{\Delta(\lambda )} \ud \lambda .
\end{equation}
The contour integral on the left of equation~\eqref{eqn:EasierContourIntegral} can be evaluated numerically more easily because the floating point error described in difficulty~(ii) will not arise.
The task is to find such a function $z$.

Idea:
Is there a way to construct a conformal map $z$ which maps the semiannulus $\Ann(0,r,R)\cap\CC^+$ to itself while shifting the point $0$ to $-3R$?
If so, is there a way that we can control the gradient of $z$ on the semiannulus in terms of $\exp[2t\Im(\lambda )\Re(\lambda )]$?
This may be a way to construct $z$.

\textcolor{blue}{
	After talking with Peter, I now think we know how to do this.
	But there may be a bigger problem.
}

\textcolor{blue}{
	Even before this becomes an issue, the exponentials in the numerator and denominator become very large.
	The ratio is not so bad, but if the exponential in the numerator is already $10^{400}$, then that is outside the set of floating point numbers \emph{regardless of the number of significant figures} because the exponent is too large.
	It doesn't matter that the denominator may be $10^{450}$ so the ratio is (within the accuracy of our calculations) zero, because the numerator cannot be evaluated in the first place.
	It is not so hard to get around this if the numerator and denominator are each single terms: just take a log of the ratio before evaluating, but it is more difficult if there are extra terms.
	We need a way to detect when a term dominates each of the numerator and denominator, simplify the expression by discarding other terms, then take a log of the ratio formulaically, evaluate numerically, then undo the log operation by taking the exponential numerically.
	This is not a simple project!
}

\section{Analytic solution representation}

\input{AnalyticSolRep.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\bibliographystyle{plain}
\bibliography{./FullBib,./dbrefs}


\end{document}
























Suppose that $f:[0,L]\to\CC$ is the box function of height $h$ between $a$ and $b$
\BES
	f(x) =
	\begin{cases}
		h & x \in(a,b), \\
		0 & \mbox{otherwise,}
	\end{cases}
\EES
for some $0 \leq a<b \leq L$.
Then
\BE
	\hat{f}(\la) = \int_0^L e^{-i\la x}f(x)\D x = \frac{2h}{\la} e^{-i\la\frac{b+a}{2}}\sin\left(\la\frac{b-a}{2}\right) = \frac{2h}{\la} e^{-i\la c}\sin\left(\la w\right),
\EE
were $w$ is half the width of the box, and $c$ is the center of the box $f$.
