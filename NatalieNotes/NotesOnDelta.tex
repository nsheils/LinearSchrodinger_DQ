% !TEX TS-program = pdflatexmk

\documentclass[11pt]{article}

\usepackage{fullpage}
\usepackage{latexsym}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath,amsthm}
\usepackage{subcaption}
\usepackage{color}
\usepackage{graphicx, graphics}
\usepackage{hyperref}
\usepackage{pstricks, pst-plot, epsfig}
\usepackage{xargs}
\usepackage{arydshln}
\setlength{\dashlinegap}{2pt}

\usepackage{verbatim}

\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Ann}{Ann}
\newcommand{\ud}{\,\mathrm{d}}
\renewcommand\Re{\operatorname{Re}}
\renewcommand\Im{\operatorname{Im}}
\newcommand{\tr}{\textcolor{red}}
\newcommand{\tb}{\textcolor{blue}}

\def\Xint#1{\mathchoice
{\XXint\displaystyle\textstyle{#1}}%
{\XXint\textstyle\scriptstyle{#1}}%
{\XXint\scriptstyle\scriptscriptstyle{#1}}%
{\XXint\scriptscriptstyle\scriptscriptstyle{#1}}%
\!\int}
\def\XXint#1#2#3{{\setbox0=\hbox{$#1{#2#3}{\int}$}
\vcenter{\hbox{$#2#3$}}\kern-.5\wd0}}
\def\ddashint{\Xint=}
\def\dashint{\Xint-}

\definecolor{green}{RGB}{0,128,0}


\newcommand{\CC}{\mathbb{C}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\la}{\lambda}      

%Set up Theorem Styles
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{conc}[thm]{Conclusion}
\newtheorem{defn}[thm]{Definition}
\newtheorem{conj}[thm]{Conjecture}
\theoremstyle{definition}
\newtheorem{cond}[thm]{Condition}
\newtheorem{asm}[thm]{Assumption}
\newtheorem{ntn}[thm]{Notation}
\newtheorem{prob}[thm]{Problem}
\theoremstyle{remark}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{eg}[thm]{Example}

\DeclareMathOperator{\erf}{erf}
\DeclareMathOperator{\sn}{sn}
\DeclareMathOperator{\cn}{cn}
\DeclareMathOperator{\dn}{dn}

\graphicspath{{gfx/}}

\newcommand{\BE}{\begin{equation}}                                 %Begin an equation environment
\newcommand{\EE}{\end{equation}}                                   %End an equation environment
\newcommand{\BES}{\begin{equation*}}                               %Begin an equation* environment
\newcommand{\EES}{\end{equation*}}                                 %End an equation* environment
\newcommand{\D}{\ensuremath{\,\mathrm{d}}}						   %an upright d for infinitesimals			               

\begin{document}

\title{UTM and DQ in Linear Schr\"odinger}
\date{\today}

\maketitle

Consider the linear Schr\"odinger equation:
\begin{subequations}\label{ls}
\begin{align}
iq_t+q_{xx}&=0, &&(x,t)\in[0,L]\times [0,T],\\
q(x,0)&=q_0(x)=\delta(x-x_0),&&x\in[0,L],\\
	q_x(L,t)=&\beta q_x(0,t)\\
	q(L,t)=&\frac{1}{\beta}q(0,t).
\end{align}
\end{subequations}

We define
\BE
	\lambda_j = \frac{1}{L}\left[2j\pi + \arccos\left( \frac{2\beta}{1+\beta^2} \right)\right].
\EE
%so that $\{\lambda\in\CC:\Delta(\la)=0\} = \{\lambda_j,-\lambda_j:j\in\ZZ\}$.
For notational convenience, we define the modulus $1$ complex number
\BE
	\gamma = \frac{2\beta}{\beta^2+1} + i\frac{|\beta^2-1|}{\beta^2+1} = e^{i\la_jL},
\EE
which is independent of $j$.

We obtain the formula
\BE
	\label{eqn:Analytics.q.formula}
	%q(x,t) = \frac{1}{2L} \sum_{j\in\ZZ} e^{-i\la_j^2t} \left[ -e^{i\la_j x} + i\sgn(\beta^2-1)\gamma e^{-i\la_j x} \right] \left(\hat{q}_0(\la_j) + \frac{i\sgn(\beta^2-1)}{\gamma}\hat{q}_0(-\la_j)\right),
	q(x,t) = \frac{1}{2L} \sum_{j\in\ZZ} e^{-i\la_j^2t} \left[ -e^{i\la_j x} + i\sgn(\beta^2-1)\gamma e^{-i\la_j x} \right] \left(e^{-i\lambda_j x_0}+ \frac{i\sgn(\beta^2-1)}{\gamma}e^{i\lambda_j x_0}\right),
\EE
which is valid for all $|\beta|\neq1$.

We restate~\cite{Olver} Lemma 2, on $[0,L]$ rather than $[0,2\pi]$:
\begin{lem}\label{lem:O2}
Given a function $f(x)$ we expand it in a Fourier series:
$$f(x)\sim \sum_{j\in\ZZ} c_j e^{i\frac{2\pi jx}{L}},$$ where $$c_j=\frac{1}{L}\int_0^L f(x)e^{-i\frac{2\pi j x}{L}}.$$  Then the $c_j$ are $k$-periodic in their indices ($c_{j+k}=c_j$ for all $j$), if and only if the series represents a linear combination of $k$ periodically extended delta functions concentrated at the nodes $x_m=\frac{Lm}{k}$ for $m=0,1,\ldots,k-1$:
$$f(x)=\sum_{m=0}^{k-1} a_m \delta\left(x-\frac{mL}{k}\right),$$ for suitable $a_0,a_1,\ldots,a_{k-1}$.
\end{lem}

Observe that~\eqref{eqn:Analytics.q.formula}, when multiplied out is comprised of four terms.  
One of which is
$$
\frac{-1}{2L}\sum_{j\in\ZZ}e^{i\lambda_j(x-x_0)-i\lambda_j^2t}.$$
For notational convenience let $a=\arccos\left( \frac{2\beta}{1+\beta^2} \right).$  
We expand to find
\begin{align}
\frac{-1}{2L}\sum_{j\in\ZZ}e^{i\lambda_j(x-x_0)-i\lambda_j^2t}&=\frac{-1}{2L}\sum_{j\in\ZZ}e^{\frac{i}{L}(2\pi j+a)(x-x_0)-\frac{i}{L^2}(2\pi j+a)^2t}\\
&=\frac{-1}{2L}\sum_{j\in\ZZ}e^{i\frac{2\pi jx}{L}}e^{\frac{-2i\pi j x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{i}{L^2}(2\pi j+a)^2t} \ud y.
\end{align}
Then, in the notation of Lemma~\ref{lem:O2}, 
$$c_j(t)=e^{\frac{-2i\pi jx_0}{L}+\frac{ia(x-x_0)}{L}-\frac{i}{L^2}(2\pi j+a)^2t}.$$

Hence, 
\begin{align}
c_{j+k}(t)=&e^{\frac{-2i\pi (j+k)x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{i}{L^2}(2\pi (j+k)+a)^2t} \\
=&e^{\frac{-2i\pi j x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{i}{L^2}(2\pi j+a)^2t} e^{\frac{-4i\pi^2 kt}{L^2}(2 j+ k)}e^{\frac{-2i\pi k y}{L}} e^{\frac{-4ia\pi kt}{L^2}}
\end{align}
When $t=\frac{pL^2}{k\pi}$ we expect the solution will quantize:
\begin{align}
c_{j+k}\left(\frac{pL^2}{k\pi}\right)=&e^{\frac{-2i\pi j x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{i}{L^2}(2\pi j+a)^2t} e^{\frac{-4i\pi^2 kt}{L^2}(2 j+ k)}e^{\frac{-2i\pi k x_0}{L}} e^{\frac{-4ia\pi kt}{L^2}} \ud y\\
=&e^{\frac{-2i\pi j x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{ip}{k\pi}(2\pi j+a)^2} e^{-4i\pi p(2 j+ k)}e^{\frac{-2i\pi k x_0}{L}} e^{-4iap} \\
=&e^{\frac{-2i\pi j x_0}{L}+\frac{ia(x-x_0)}{L}-\frac{ip}{k\pi}(2\pi j+a)^2} e^{\frac{-2i\pi k x_0}{L}} e^{-4iap} \\
=&c_j\left(\frac{pL^2}{k\pi}\right)e^{\frac{-2i\pi k x_0}{L}} e^{-4iap}
\end{align}
\textcolor{blue}{
This is very close to $c_j\left(\frac{pL^2}{k\pi}\right)=c_{j+k}\left(\frac{pL^2}{k\pi}\right)$.  ($x_0=Lm/k$ meaning the delta functions are concentrated at nodes for $m=0,1,\ldots,k-1$ just as before.) Especially when we recall that $a=\arccos(2\beta/(1+\beta^2))$ which is $\pi/2$ when $\beta=0$, $\pi$ at $\beta=-1$, $0$ at $\beta=1$ and approaches $\pi/2$ for $|\beta|$ large (actually it approaches $\pi/2$ very quickly).  
}

\textcolor{red}{IDEA: don't separate out the $e^{i\frac{2\pi j x}{L}}$ term because we need that for translational symmetry.  Instead, keep it and see what happens.  We will also need to prove a new version of~\ref{lem:O2}.}

\end{document}
