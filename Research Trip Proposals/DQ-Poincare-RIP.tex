% Preamble
\documentclass[10pt]{article}

\usepackage{amssymb,amsmath,mathrsfs,amsthm,latexsym,hyperref}
\usepackage[a4paper,includeheadfoot,left=20mm,right=20mm,top=15mm,bottom=25mm]{geometry} %sets up the margins
%\usepackage{rotating}
\usepackage{verbatim}
\usepackage{xcolor,graphicx}
\usepackage[official]{eurosym}

\setcounter{MaxMatrixCols}{15}

%Set up Theorem Styles
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{conc}[thm]{Conclusion}
\newtheorem{defn}[thm]{Definition}
\newtheorem{conj}[thm]{Conjecture}
\theoremstyle{definition}
\newtheorem{cond}[thm]{Condition}
\newtheorem{asm}[thm]{Assumption}
\newtheorem{ntn}[thm]{Notation}
\newtheorem{prob}[thm]{Problem}
\theoremstyle{remark}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{eg}[thm]{Example}

%Mathmode shortcuts
\newcommand{\TextNorm}[1]{\textrm{\textmd{\textup{#1}}}}
\newcommand{\hsforall}{\hspace{1mm}\forall\hspace{1mm}}			   %Spacing around \forall
\newcommand{\hsexists}{\hspace{1mm}\exists\hspace{1mm}} 		   %Spacing around \exists
\newcommand{\Trace}{\operatorname*{Tr}}                            %Trace of something, eg a contour
\newcommand{\res}{\operatorname*{Res}}                             %Residue of an expression
\newcommand{\PV}{\operatorname*{PV}}                               %Principal Value of an integral
\newcommand{\ord}{\operatorname*{ord}}                             %Order of a boundary form
\newcommand{\Span}{\operatorname*{span}}                           %Linear span of a set
\newcommand{\Clos}{\operatorname*{clos}}                           %Closure of a set
\renewcommand{\Re}{\operatorname*{Re}}                             %Real part of something
\renewcommand{\Im}{\operatorname*{Im}}                             %Imaginary part of something
\newcommand{\dist}{\operatorname*{dist}}  						   %dist function	
\newcommand{\D}{\ensuremath{\,\mathrm{d}}}						   %an upright d for infinitesimals			               
\newcommand{\T}{\text{\TextNorm{T}}\hspace{0.1mm}}				   %an upright T for matrix transposes
\newcommand{\sgn}{\operatorname*{sgn}}						       %Sign of a real number or permutation
\newcommand{\Ymax}{Y_{\mathrm{max}}}                               %And index set Y_{max}
\newcommand{\DeltaP}{\Delta_{\mathrm{PDE}}\hspace{0.5mm}}          %The function whose zeros form the PDE discrete spectrum
\newcommand{\DeltaPsup}[1]{\Delta_{\mathrm{PDE}}^{#1}\hspace{0.5mm}}%The function whose zeros form the PDE discrete spectrum, with a superscript argument
\newcommand{\DeltaPstar}{\DeltaPsup{\star}}                        %The function whose zeros form the adjoint PDE discrete spectrum
\newcommand{\DeltaPprime}{\Delta'_{\mathrm{PDE}}\hspace{0.5mm}}    %The function whose zeros form the PDE discrete spectrum for the problem \Pi'
\newcommand{\Mspacer}{\hspace{0.5mm}}                              %Spacer for below Matrix display functions
\newcommand{\M}[3]{#1_{#2\Mspacer#3}}                              %Print a symbol with two subscripts eg a matrix entry
\newcommand{\Msup}[4]{#1_{#2\Mspacer#3}^{#4}}                      %Print a symbol with two subscripts and a superscript eg a matrix entry
\newcommand{\Msups}[5]{#1_{#2\Mspacer#3}^{#4\Mspacer#5}}           %Print a symbol with two subscripts and two superscripts eg a matrix entry
\newcommand{\MAll}[7]{\prescript{#1}{#2}{#3}_{#4\Mspacer#5}^{#6\Mspacer#7}}           %Print a symbol with two subscripts and two superscripts eg a matrix entry
\renewcommand{\geq}{\geqslant}                                     %Always use \geqslant, never the standard \geq
\renewcommand{\leq}{\leqslant}                                     %Always use \leqslant, never the standard \leq
\renewcommand{\epsilon}{\varepsilon}							   %Always use varepsilon
\newcommand{\BE}{\begin{equation}}                                 %Begin an equation environment
\newcommand{\EE}{\end{equation}}                                   %End an equation environment
\newcommand{\be}{\begin{equation}}                                 %Begin an equation environment
\newcommand{\ee}{\end{equation}}                                   %End an equation environment
\newcommand{\BES}{\begin{equation*}}                               %Begin an equation* environment
\newcommand{\EES}{\end{equation*}}                                 %End an equation* environment
\newcommand{\BP}{\begin{pmatrix}}                                  %Begin a pmatrix environment
\newcommand{\EP}{\end{pmatrix}}                                    %End a pmatrix environment
\newcommand{\Iff}{\ensuremath{\Leftrightarrow}}                    %Shorthand for bidirectional implication
\newcommand{\NN}{\mathbb{N}}                                        %Shorthand for the set of natural numbers
\newcommand{\ZZ}{\mathbb{Z}}                                        %Shorthand for the set of integers
\newcommand{\QQ}{\mathbb{Q}}                                        %Shorthand for the set of rational numbers
\newcommand{\RR}{\mathbb{R}}                                        %Shorthand for the set of real numbers
\newcommand{\CC}{\mathbb{C}}                                        %Shorthand for the set of complex numbers
\newcommand{\la}{\lambda}      
\newcommand\re{{\rm e}}     
\providecommand{\MAT}[1]{\mathbf{#1}}
\providecommand{\MATscr}[1]{\pmb{\mathscr{#1}}}

%Bigger than \Bigg
\makeatletter
	\newcommand{\biggg}{\bBigg@{3}}
	\newcommand{\Biggg}{\bBigg@{4}}
	\newcommand{\bigggg}{\bBigg@{5}}
	\newcommand{\Bigggg}{\bBigg@{6}}
\makeatother

%Superscripts in text
\newcommand{\superscript}[1]{\ensuremath{^{\textrm{#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{#1}}}}
\newcommand{\Thns}[0]{\superscript{th}}
\newcommand{\Th}[0]{\Thns~}
\newcommand{\stns}[0]{\superscript{st}}
\newcommand{\st}[0]{\stns~}
\newcommand{\ndns}[0]{\superscript{nd}}
\newcommand{\nd}[0]{\ndns~}
\newcommand{\rdns}[0]{\superscript{rd}}
\newcommand{\rd}[0]{\rdns~}

%Macro \mathclap{} to remove the horizontal bounding box around a mathematical element.
\def\clap#1{\hbox to 0pt{\hss#1\hss}}
\def\mathclap{\mathpalette\mathclapinternal}
\def\mathclapinternal#1#2{\clap{$\mathsurround=0pt#1{#2}$}}

%Number equations by section
%\numberwithin{equation}{section}
%\renewcommand{\theequation}{\thesection.\arabic{equation}}

%Width calculations
\usepackage{calc}

%Hyphenation
\hyphenation{non-zero}

\title{Dispersive quantisation via the unified transform method}
\date{\vspace{-5ex}}

% Document
\begin{document}

\maketitle

\vspace{-.5in}
\section*{Proposed Project}

Consider the periodic initial-boundary value problem
\begin{gather} \notag
	[\partial_t-\partial_x^3]q(x,t) = 0, \qquad\qquad\qquad (x,t) \in (0,2\pi)\times(0,\infty), \\ \label{eqn:IBVP}
	q(x,0) = q_0(x), \qquad\qquad\qquad \partial_x^j q(0,t) = \partial_x^j q(2\pi,t), \qquad j=0,1,2,
\end{gather}
with $q_0$ a piecewise constant initial profile.
It was observed by Olver in 2010~\cite{Olver} that this dispersive problem exhibits ``quantization''.
At time $t$ a rational multiple of $\pi$, the solution $q(\cdot,t)$ assumes a piecewise constant form, while at other times it is a fractal limit of such functions.
% The numerical demonstration in figures~\ref{fig:OlverIrrationalTime} and~\ref{fig:OlverRationalTime} were obtained as an evaluation of the Fourier series representation of the solution.
Numerical demonstrations were obtained as an evaluation of the Fourier series representation of the solution.
In the same work, an analytic explanation of the phenomenon which relied on analysis of the Fourier series was provided.

% Surprising as these results were, the novelty is their appearance for the third order dispersive equation, rather than rational time quantization in general.
Olver was the first to notice rational time quantization for a third order dispersive equation, but the phenomenon had been observed before in another context.
Indeed, similar results for the Schr\"{o}dinger equation have been studied since the early 1990's by Michael Berry and collaborators,~\cite{BMS2001a}, who named it the ``Talbot effect'' after an 1836 experiment by the nineteenth century scientist Fox Talbot~\cite{Tal1836a}, a pioneer of photography.
The phenomenon of rational time quantization in the Schr\"{o}dinger equation is well understood~\cite{BK1996a}, on a variety of domains~\cite{Tay2003a}, under most standard boundary conditions, although the case of Robin conditions remains incompletely understood.  

%Oskolkov,~\cite{Osk1992a}, extended the analysis to the third order case; extensions of the fractal analysis to nonlinear systems like the nonlinear Schr\"odinger and Korteweg-deVries equations  was recently done in~\cite{CET2015a} explaining some of the numerical investigations of Chen and Olver,~\cite{CO2012a}. 
On extension of the third order dispersive quantization results beyond the periodic domain, the difficulties of performing numerical experiments are noted~\cite{Olver}.
Due to the categorically different results for rational and irrational times, any numerical method that relies upon time-stepping will have trouble capturing the effect.
Therefore, a numerical method that relies purely upon evaluation of a solution representation obtained analytically is preferable.
The recent unified transform method (UTM) due to Fokas and collaborators~\cite{Fok2008a, FP2001a} provides such analytic solution representations.

The UTM provides a generalization of the spatial Fourier transform technique; the method tailors a transform inverse transform pair to the specific boundary conditions of the problem~\cite{FS2016a}.
In contrast to classical methods, the solution may be represented by either a series or an integral, enabling solution of initial-boundary value problems even in the case that the eigenfunctions of the spatial differential operator are not complete~\cite{Pel2005a}.
For example, the UTM yields the solution of problem~\eqref{eqn:IBVP} expressed as
\BES
	q(x,t) = \int_{\Gamma^+} \re^{i\la x - i \la^3t} \frac{\zeta^+(\la;q_0)}{\Delta(\la)} \D\la + \int_{\Gamma^-} \re^{i\la (x-1) - i \la^3t} \frac{\zeta^-(\la;q_0)}{\Delta(\la)} \D\la,
\EES
where $\Gamma^\pm$ are certain complex contours, $\zeta^\pm$ are linear combinations of Fourier transforms of the initial datum $q_0$, and the denominator $\Delta$ is Birkhoff's characteristic determinant of the spatial differential operator~\cite{Bir1908a,FS2016a}.
The particularly simple dependence of the integrands on $(x,t)$ makes this an attractive solution representation for efficient numerical evaluation.
Moreover, extension to more complicated boundary conditions changes only $\Delta$ and $\zeta^\pm$, preserving the valuable numerical qualities~\cite{KPPS2017a}.

Integral solution representations obtained via the UTM are, in some cases, the only available, and in other cases may easily be adapted to provide series solution representations both classical and novel.
Therefore this method provides the best hope for generalising the currently available results both numerically and analytically.
Moreover, the UTM has an analogue for integrable nonlinear equations including the nonlinear Schr\"{o}dinger and Korteweg-deVries equations, in which quantization is observed.

\subsection*{Aims}

\begin{enumerate}
	\item{
		Complete the numerical and analytic classification of initial-boundary value problems for the linear Schr\"{o}dinger equation according to the presence of a Talbot effect.
	}
	\item{
		Extend the above results to rational quantization for the third order dispersive equation under more general boundary conditions.
	}
	\item{
		Begin investigating how to extend the analysis to linear dispersive integro-differential equations with non-polynomial dispersion relations, arising in fluid mechanics, elasticity, DNA modeling, and elsewhere. 
		Early analytical work to prove fractal dimension has been done~\cite{OskolkovV} as well as numerical investigations~\cite{ChenOlver, Olver}.
		Some of these numerical observations were subsequently rigorously confirmed in papers of Chousionis, Erdo{\u{g}}an, Tzirakis, and Shakan~\cite{ChousionisErdoganTzirakis, ErdoganShakan, ErdoganTzirakis, ErdoganTzirakis2016}, but much more work remains to be completed, including extensions to other types of boundary conditions.  
		In the case of non-polynomial dispersion relations such as those arising in the Benjamin-Ono and Boussinesq equations, the quantized solutions are no longer piecewise constant. 
		Thus, an additional challenge is to characterize the shape of these profiles.
		%Rigorous explanations of the variety of phenomena observed in the preliminary numerical investigations in \cite{CO2012a} remains open.
	}
\end{enumerate}

\noindent
Beyond the specific aims of this proposal it is expected that the analysis may be further extended to cover integrable nonlinear equations, including the nonlinear Schr\"{o}dinger and Korteweg-deVries equations.
Another long term goal is the spectral classification of spatial differential operators admitting rational time quantization.

\section*{Participants}
Peter Olver (University of Minnesota), Beatrice Pelloni (Heriot-Watt University), Natalie Sheils (University of Minnesota), and Dave Smith (Yale-NUS College).
%\begin{description}
%	\item[Peter Olver (University of Minnesota)]{
%		was the first to observe rational time quantization of a range of dispersive initial-boundary value problems, including~\eqref{eqn:IBVP}, both linear and nonlinear.
%		He has since authored several more papers on the subject, including a recent paper with Sheils~\cite{OS2017a}.
%	}
%	\item[Beatrice Pelloni (Heriot-Watt University)]{
%		is an expert in the UTM, having given the first general implementation for problems on the finite interval~\cite{FP2001a}.
%		Pelloni and Smith collaborated on~\cite{KPPS2017a}, the first systematic work on the numerical evaluation of integral solution representations obtained through the UTM.
%	}
%	\item[Natalie Sheils (University of Minnesota)]{
%		extended the UTM to admit interface problems, including second order problems in collaboration with Pelloni~\cite{DPS2014a}, and third order problems with Smith~\cite{DSS2016a}.
%		Recently, Sheils and Smith obtained numerical and analytic evidence of the Talbot effect in finite interval domains with more complex boundary conditions (in preparation).
%	}
%	\item[David Smith (Yale-NUS College)]{
%		implemented the finite interval UTM for the most general boundary conditions~\cite{Smi2012a} and described the spectral theory of associated differential operators~\cite{FS2016a}.
%	}
%\end{description}
\medskip

\noindent
In addition to those works mentioned above, the participants have collaborated on~\cite{OlverSheils, PS2013a,PS2016a,PelloniSmith2018,SS2015a}.

\section*{Budget and Desired Period}
We plan to meet for ten days beginning 13 May 2019.
We estimate a total cost of \euro{}6228.00 which includes round-trip flights for each of the four participants as well as seven nights in one of the suggested hotels.


\bibliographystyle{amsplain}
{\small\bibliography{../dbrefs,../FullBib}}

\end{document}