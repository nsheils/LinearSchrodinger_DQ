% !TEX TS-program = pdflatexmk

\documentclass[11pt]{article}

\input{texHead}

\begin{document}

\title{The Talbot effect in linear Schr\"odinger with pseudoperiodic boundary conditions. Supplementary material.}
\date{\today}
\author{}

\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
\textcolor{red}{Write something}
\end{abstract}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\textcolor{red}{Describe contents}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multidimensional periodic problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the main work, it was claimed that
\BE
    q(\BVec{x},t) = \prod_{k=1}^d L_k^{-1} \sum_{\substack{j_k=-\infty:\\k\in\{1,2,\ldots,d\}}}^\infty \exp\left( -it\sum_{k=1}^d\la_{j_k}^2 \right) \exp\left( i\sum_{k=1}^d\la_{j_k}x_k \right) \hat{q}_0(\la_{j_1},\la_{j_2},\ldots,\la_{j_d}).
\EE
is the solution of problem
\begin{subequations} \label{eqn:MultidimIBVP}
\begin{align}
    \label{eqn:MultidimIBVP.PDE}
    iq(\BVec{x},t) + \nabla^2q(\BVec{x},t) &= 0 & (\BVec{x},t) &\in [\BVec{0},\BVec{L}]\times[0,T], \\
    \label{eqn:MultidimIBVP.IC}
    q(\BVec{x},0) &= q_0(\BVec{x}) & \BVec{x} &\in [\BVec{0},\BVec{L}], \\
    \label{eqn:MultidimIBVP.BC}
    \left.\partial_{x_k}^jq(\BVec{x},t)\right\rvert_{x_k=0} &= \left.\partial_{x_k}^jq(\BVec{x},t)\right\rvert_{x_k=L_k}, & (x_1,\ldots,x_{k-1},x_{k+1},\ldots,x_d,t) &\in \prod_{\substack{r=1\\r\neq k}}^d[0,L_r]\times[0,T],
\end{align}
\end{subequations}
for each $k\in\{1,2,\ldots,d\}$ and each $j\in\{0,1\}$.
In this section, we give the full argument.

\subsection{Separation of variables}

We seek separated solutions
\BES
    q(\BVec{x},t) = T(t)\prod_{k=1}^d X_k(x_k)
\EES
of equations~\eqref{eqn:MultidimIBVP.PDE} and \eqref{eqn:MultidimIBVP.BC}.
The PDE implies
\BES
    i\frac{T'}{T} + \sum_{k=1}^d \frac{X_k''}{X_k} = 0.
\EES
As each of these ratios is a function of a different independent variable, it must be that each is a constant.
We denote these constants $\rho$ and $\la^{\{k\}}$ so that
\BES
    \frac{X_k''}{X_k} = - \la^{{\{k\}}^2}, \qquad i\frac{T'}{T} = \rho = \sum_{k=1}^d \la^{{\{k\}}^2}.
\EES

It is immediate that
\BES
    T(t) = e^{i\rho t},
\EES
and each $X_k$ satisfies the periodic Sturm-Liouville problem on $(0,L_k)$
\begin{align*}
    X_k''(x_k) + \la^{{\{k\}}^2}X_k(x_k) &= 0, \\
    X_k(0) &= X_k(L_k), \\
    X_k'(0) &= X_k'(L_k).
\end{align*}
The solution of the Sturm-Liouville problem is given by the eigenfunctions and corresponding eigenvalues
\BES
    X_k(x_k) = A\cos(\la x_k) + B\sin(\la x_k), \qquad \la^{\{k\}} = \frac{2\pi j}{L_k}, \qquad j \in \NN_0,
\EES
in which the arbitrary constants $A,B$ represent the fact that each eigenspace (other than that for eigenvalue $0$) has dimension $2$.

An arbitrary linear combination of these solutions has the form
\begin{subequations} \label{eqn:MultidimSolution1}
\begin{align}
    q(\BVec{x},t) &= \sum_{\substack{j_k=0:\\k\in\{1,2,\ldots,d\}}}^\infty \exp\left(-it\sum_{k=1}^d\la_{j_k}^2\right) \sum_{\substack{r_k=0:\\k\in\{1,2,\ldots,d\}}}^1 A_{j_1,j_2,\ldots,j_d}^{r_1,r_2,\ldots,r_d} \prod_{k=1}^d \cos\left( \la_{j_k}x_k-\frac{\pi r_k}{2} \right), \\
    \la_{j_k} &= \frac{2\pi j}{L_k}, \\
    A_{j_1,j_2,\ldots,j_d}^{r_1,r_2,\ldots,r_d} &= \prod_{\substack{k=1:\\j_k>0}}^{d}2 \prod_{k=1}^d L_k^{-1} \int_{\BVec{0}}^{\BVec{L}} \prod_{k=1}^d \cos\left( \la_{j_k}y_k-\frac{\pi r_k}{2} \right) q_0(\BVec{y}) \,\BVec{dy},
\end{align}
\end{subequations}
in which the $d$ length binary string $\BVec{r}$ is used to select between sine and cosine eigenfunctions.

\subsection{Alternative series representation of solution}

For efficiency of presentation, we rewrite equations~\eqref{eqn:MultidimSolution1} using vector notation,
\begin{subequations} \label{eqn:MultidimSolution2}
\begin{align}
    \label{eqn:MultidimSolution2.q}
    q(\BVec{x},t) &= \sum_{\BVec{j}\in\NN_0^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \sum_{\BVec{r}\in\{0,1\}^d} A_{\BVec{j}}^{\BVec{r}} \prod_{k=1}^d \cos\left( \la_{j_k}x_k-\frac{\pi r_k}{2} \right), \\
    \label{eqn:MultidimSolution2.laj}
    \bm{\la}_{\BVec{j}} &= \left( \la_{j_1} , \ldots , \la_{j_d} \right) = \left( \frac{2\pi j_1}{L_1} , \ldots , \frac{2\pi j_d}{L_d} \right), \\
    \label{eqn:MultidimSolution2.A}
    A_{\BVec{j}}^{\BVec{r}} &= \prod_{\substack{k=1:\\j_k>0}}^{d}2 \prod_{k=1}^d L_k^{-1} \int_{\BVec{0}}^{\BVec{L}} \prod_{k=1}^d \cos\left( \la_{j_k}y_k-\frac{\pi r_k}{2} \right) q_0(\BVec{y}) \,\BVec{dy}.
\end{align}
\end{subequations}
Substituting equation~\eqref{eqn:MultidimSolution2.A} into equation~\eqref{eqn:MultidimSolution2.q} and expanding the sines and cosines as exponentials, we obtain
\begin{multline*}
    q(\BVec{x},t) = \sum_{\BVec{j}\in\NN_0^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \sum_{\BVec{r}\in\{0,1\}^d} \frac{\prod_{\substack{k=1:\\j_k>0}}^{d}2}{2^{2d} \prod_{k=1}^d L_k i^{\sum_{k=1}^dr_k+\sum_{k'=1}^dr_{k'}} } \int_{\BVec{0}}^{\BVec{L}} q_0(\BVec{y}) \\
    \prod_{k=1}^d \left( e^{i\la_{j_k}x_k} + (-1)^{r_k} e^{-i\la_{j_k}x_k} \right) \prod_{k'=1}^d \left( e^{i\la_{j_{k'}}y_{k'}} + (-1)^{r_{k'}} e^{-i\la_{j_{k'}}y_{k'}} \right)  \,\BVec{dy}.
\end{multline*}
The two products in the integrand, each of $d$ factors containing $2$ terms, may be combined into a single product, of $d$ factors containing $4$ terms, to yield
\begin{multline} \label{eqn:MultidimSolution3}
    q(\BVec{x},t) = \sum_{\BVec{j}\in\NN_0^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \Bigg[ \sum_{\BVec{r}\in\{0,1\}^d} \frac{\prod_{\substack{k=1:\\j_k>0}}^{d}2}{2^{2d} \prod_{k=1}^d L_k (-1)^{\sum_{k=1}^dr_k} } \int_{\BVec{0}}^{\BVec{L}} q_0(\BVec{y}) \\
    \prod_{k=1}^d \left( e^{i\la_{j_k}(x_k+y_k)} + (-1)^{r_k} e^{i\la_{j_k}(x_k-y_k)} + (-1)^{r_{k}} e^{i\la_{j_k}(-x_k+y_k)} + e^{i\la_{j_k}(-x_k-y_k)} \right)  \,\BVec{dy} \Bigg].
\end{multline}

For any choice of $\kappa\in\{1,2,\ldots,d\}$, let $K=\{1,2,\ldots,d\}\setminus\{\kappa\}$.
In the bracketed part of equation~\eqref{eqn:MultidimSolution3}, we expand the sum over $r_\kappa$ to obtain
\begin{multline*}
    \sum_{\substack{r_k=0:\\k\in K}}^1 \frac{\prod_{\substack{k=1:\\j_k>0}}^{d}2}{2^{2d} \prod_{k=1}^d L_k (-1)^{2\sum_{k\in K}r_k}} \int_{\BVec{0}}^{\BVec{L}} q_0(\BVec{y}) \\
    \prod_{k\in K} \left( e^{i\la_{j_k}(x_k+y_k)} + (-1)^{r_k} e^{i\la_{j_k}(x_k-y_k)} + (-1)^{r_{k}} e^{i\la_{j_k}(-x_k+y_k)} + e^{i\la_{j_k}(-x_k-y_k)} \right) \\
    \Bigg\{ \frac{1}{(-1)^0} \left( e^{i\la_{j_\kappa}(x_\kappa+y_\kappa)} + (-1)^{0} e^{i\la_{j_\kappa}(x_\kappa-y_\kappa)} + (-1)^{0} e^{i\la_{j_\kappa}(-x_\kappa+y_\kappa)} + e^{i\la_{j_\kappa}(-x_\kappa-y_\kappa)} \right) \\
    + \frac{1}{(-1)^1} \left( e^{i\la_{j_\kappa}(x_\kappa+y_\kappa)} + (-1)^{1} e^{i\la_{j_\kappa}(x_\kappa-y_\kappa)} + (-1)^{1} e^{i\la_{j_\kappa}(-x_\kappa+y_\kappa)} + e^{i\la_{j_\kappa}(-x_\kappa-y_\kappa)} \right) \Bigg\} \,\BVec{dy},
\end{multline*}
in which the braced factor simplifies to
\BES
    2\left( e^{i\la_{j_\kappa}(x_\kappa-y_\kappa)} + e^{i\la_{j_\kappa}(-x_\kappa+y_\kappa)} \right).
\EES
This argument was independent of choice of dimension $\kappa\in\{1,2,\ldots,d\}$, so
\BE \label{eqn:MultidimSolution4}
    q(\BVec{x},t) = \sum_{\BVec{j}\in\NN_0^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \frac{\prod_{\substack{k=1:\\j_k>0}}^{d}2}{2^{d} \prod_{k=1}^d L_k} \int_{\BVec{0}}^{\BVec{L}} q_0(\BVec{y}) \prod_{k=1}^d \left( e^{i\la_{j_k}(x_k-y_k)} + e^{i\la_{j_k}(-x_k+y_k)} \right)  \,\BVec{dy}.
\EE

We expand the integral in equation~\eqref{eqn:MultidimSolution4} as
\begin{align*}
    \int_{\BVec{0}}^{\BVec{L}} \cdots \BVec{dy}
    &= \sum_{\BVec{s}\in\{0,1\}^d} \int_{\BVec{0}}^{\BVec{L}} q_0(\BVec{y}) \exp\left(i\sum_{k=1}^d\la_{j_k}(-1)^{s_k}(x_k-y_k)\right) \,\BVec{dy} \\
    &= \sum_{\BVec{s}\in\{0,1\}^d} \exp\left(i\sum_{k=1}^d\la_{j_k}(-1)^{s_k}x_k\right) \int_{\BVec{0}}^{\BVec{L}} \exp\left(-i\sum_{k=1}^d\la_{j_k}(-1)^{s_k}y_k\right) q_0(\BVec{y}) \,\BVec{dy} \\
    &= \sum_{\BVec{s}\in\{0,1\}^d} \exp\left(i\sum_{k=1}^d\la_{j_k}(-1)^{s_k}x_k\right) \hat{q}_0\left( (-1)^{s_1}\la_{j_1} , (-1)^{s_2}\la_{j_2} , \ldots , (-1)^{s_n}\la_{j_n} \right),
\end{align*}
because the exponent in the integrand of the penultimate line is the dot product
\BES
    -i \left( (-1)^{s_1}\la_{j_1} , (-1)^{s_2}\la_{j_2} , \ldots , (-1)^{s_n}\la_{j_n} \right) \cdot \BVec{y}.
\EES
Making this substitution and interchanging sums in equation~\eqref{eqn:MultidimSolution4}, we obtain
\begin{multline} \label{eqn:MultidimSolution5}
    q(\BVec{x},t) = \prod_{k=1}^d L_k^{-1} \sum_{\BVec{s}\in\{0,1\}^d} \sum_{\BVec{j}\in\NN_0^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \frac{\prod_{\substack{k=1:\\j_k>0}}^{d}2}{2^{d}} \exp\left(i\sum_{k=1}^d\la_{j_k}(-1)^{s_k}x_k\right) \\
    \hat{q}_0\left( (-1)^{s_1}\la_{j_1} , (-1)^{s_2}\la_{j_2} , \ldots , (-1)^{s_n}\la_{j_n} \right).
\end{multline}

For any choice of $\kappa\in\{1,2,\ldots,d\}$, we define $K=\{1,2,\ldots,d\}\setminus\{\kappa\}$ and explicitly expand the sum in $s_\kappa$:
\begin{multline*}
    q(\BVec{x},t) = \prod_{k=1}^d L_k^{-1} \sum_{\substack{s_k=0:\\k\in K}}^1 \sum_{\substack{j_k\in\NN_0:\\k\in K}} \exp\left(-it \sum_{k\in K}\la_{j_k}^2 \right) \frac{\prod_{\substack{k\in K:\\j_k>0}}2}{2^{d-1}} \exp\left(i\sum_{k\in K}\la_{j_k}(-1)^{s_k}x_k\right) \\
    \sum_{j_\kappa\in\NN_0} \exp\left(-it \la_{j_\kappa}^2 \right) \frac{\prod_{\substack{k=\kappa:\\j_k>0}}^{\kappa}2}{2} \left[ \exp\left(i\la_{j_\kappa}x_\kappa\right) \hat{q}_0\left( \ldots , \la_{j_\kappa} , \ldots \right) + \exp\left(-i\la_{j_\kappa}x_\kappa\right) \hat{q}_0\left( \ldots , -\la_{j_\kappa} , \ldots \right) \right],
\end{multline*}
where the final product evaluates to $2$ if $j_\kappa>0$, but evaluates to $1$ if $j_\kappa=0$.
By equation~\eqref{eqn:MultidimSolution2.laj}, $-\la_{j_\kappa} = \la_{-j_\kappa}$.
Therefore, the second line simplifies to
\BE
    \sum_{j_\kappa\in\ZZ} \exp\left(-it \la_{j_\kappa}^2 \right) \exp\left(i\la_{j_\kappa}x_\kappa\right) \hat{q}_0\left( \ldots , \la_{j_\kappa} , \ldots \right).
\EE
This argument was independent of choice of dimension $\kappa\in\{1,2,\ldots,d\}$, so
\BE \label{eqn:MultidimSolution6}
    q(\BVec{x},t) = \prod_{k=1}^d L_k^{-1} \sum_{\BVec{j}\in\ZZ^d} \exp\left(-it\bm{\la}_{\BVec{j}}\cdot\bm{\la}_{\BVec{j}}\right) \exp\left( i \bm{\la}_{\BVec{j}} \cdot \BVec{x} \right) \hat{q}_0\left( \bm{\la}_{\BVec{j}} \right).
\EE



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\bibliographystyle{plain}
\bibliography{./FullBib,./dbrefs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}







